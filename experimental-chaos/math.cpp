#ifndef MUH_MATH
#define MUH_MATH

#include <cstdio>
#include <cstring>
#include <limits>
#include <math.h>
#include <random>
#include <stdint.h>
#include <immintrin.h>

// Constants
static constexpr float infinity = std::numeric_limits<float>::infinity();
static constexpr float pi = 3.1415926535897932385f;
static constexpr float epsilon = 1e-8;

// Utility Functions
inline float random_float()
{
#if 0
    static std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
    static std::mt19937 generator;
    return distribution(generator);
#else
    // goodol' c random is faster...
    return rand() / (RAND_MAX + 1.0);
#endif
}

inline float random_float(float min, float max)
{
    return min + (max-min) * random_float();
}

inline int random_int(int min, int max)
{
    return static_cast<int>(random_float(min, max));
}

inline float degrees_to_radians(float degrees)
{
    return degrees * pi / 180.0f;
}

inline float clamp(float value, float lower, float upper)
{
    return value < lower ? lower : (value > upper ? upper : value);
}

union vec3
{
    struct {
        float x, y, z;
    };
    struct {
        float r, g, b;
    };
    float e[3];

    static vec3 one()   { return vec3(1, 1, 1); }
    static vec3 zero()  { return vec3(0, 0, 0); }
    static vec3 red()   { return vec3(1, 0, 0); }
    static vec3 green() { return vec3(0, 1, 0); }
    static vec3 blue()  { return vec3(0, 0, 1); }

    static vec3 random()
    {
        return vec3(random_float(), random_float(), random_float());
    }

    static vec3 random(float min, float max)
    {
        return vec3(random_float(min, max), random_float(min, max), random_float(min, max));
    }

    vec3() { }

    vec3(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    float length() const
    {
        return sqrt(length_squared());
    }

    float length_squared() const
    {
        return x*x + y*y + z*z; 
    }

    vec3& normalize()
    {
        *this /= length();
        return *this;
    }

    vec3 normal() const
    {
        vec3 result = *this;
        result.normalize();
        return result;
    }

    char const* c_str(char* buffer = NULL) const
    {
        constexpr char const* format = "(%.2f, %.2f, %.2f)";

        if (buffer == NULL)
        {
            constexpr int internal_buffer_size = 1024;
            static char* internal_buffer = new char[internal_buffer_size];
            static int current_idx = 0;

            // "circular" buffer with some safe margin
            if (current_idx + strlen(format)*2 > internal_buffer_size)
            {
                current_idx = 0;
            }

            buffer = internal_buffer + current_idx;
            int chars_written = sprintf(buffer, format, x, y, z);
            current_idx += chars_written + 1;
#if DEBUG_VEC3_C_STR || 0
            printf("\n\n\n===== vec3::c_str()\n");
            printf("internal_buffer: %ld\n", (long)internal_buffer);
            printf("buffer         : %ld\n", (long)buffer);
            printf("current_idx    : %d\n", (int)current_idx);
            printf("=====\n\n");
#endif
        }
        else
        {
            sprintf(buffer, format, x, y, z);
        }
        
        return buffer;
    }

    inline float operator[](int axis) const
    {
        assert(0 <= axis && axis < 3);
        return e[axis];
    }

    inline float& operator[](int axis)
    {
        assert(0 <= axis && axis < 3);
        return e[axis];
    }

    inline vec3& operator+=(vec3 const& other)
    {
        x += other.x;
        y += other.y;
        z += other.z;
        return *this;
    }

    inline vec3& operator-=(vec3 const& other)
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        return *this;
    }

    inline vec3& operator*=(vec3 const& other)
    {
        x *= other.x;
        y *= other.y;
        z *= other.z;
        return *this;
    }

    inline vec3 operator*=(float s)
    {
        x *= s;
        y *= s;
        z *= s;
        return *this;
    }

    inline vec3 operator/=(float s)
    {
        x /= s;
        y /= s;
        z /= s;
        return *this;
    }

    inline bool near_zero()
    {
        const auto s = 1e-8;
        return (fabs(x) < s) && (fabs(y) < s) && (fabs(z) < s);
    }
};

typedef vec3 color;

inline vec3 operator+(vec3 const& a, vec3 const& b)
{
    return vec3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline vec3 operator-(vec3 const& v)
{
    return vec3(-v.x, -v.y, -v.z);
}

inline vec3 operator-(vec3 const& a, vec3 const& b)
{
    return vec3(a.x - b.x, a.y - b.y, a.z - b.z);
}

inline vec3 operator*(vec3 const& a, vec3 const& b)
{
    return vec3(a.x * b.x, a.y * b.y, a.z * b.z);
}

inline vec3 operator*(vec3 const& v, float s)
{
    return vec3(v.x * s, v.y * s, v.z * s);
}

inline vec3 operator*(float s, vec3 const& v)
{
    return v * s;
}

inline vec3 operator/(vec3 const& v, float s)
{
    return vec3(v.x / s, v.y / s, v.z / s);
}

inline float dot(vec3 const& u, vec3 const& v)
{
    return u.x * v.x + u.y * v.y + u.z * v.z;
}

inline vec3 cross(vec3 const& u, vec3 const& v)
{
    return vec3(u.y * v.z - u.z * v.y,
                u.z * v.x - u.x * v.z,
                u.x * v.y - u.y * v.x);
}

inline vec3 unit_vector(vec3 const& v)
{
    return v / v.length();
}

#define VEC_RANDOM_LOOP 0

inline vec3 random_in_unit_sphere()
{
#if VEC_RANDOM_LOOP
    printf("loop\n");
    for (;;)
    {
        vec3 p = vec3::random(-1,1);

        if (p.length_squared() >= 1)
        {
            continue;
        }

        assert(p.length() <= 1.0f);
        return p;
    }
#else 
    vec3 p = vec3::random();
    p = (p / p.length()) * random_float();
    assert(p.length() <= 1.0f);
    return p;
#endif
}

inline vec3 random_unit_vector()
{
    return unit_vector(vec3::random());
}

inline vec3 random_in_hemisphere(vec3 const& normal)
{
    vec3 in_unit_sphere = random_in_unit_sphere();
    if (dot(in_unit_sphere, normal) > 0.0)
    {
        // In the same hemisphere as the normal
        return in_unit_sphere;
    }
    else
    {
        return -in_unit_sphere;
    }
}

inline vec3 reflect(vec3 const& v, vec3 const& n)
{
    return v - 2 * dot(v,n) * n;
}

vec3 refract(vec3 const& uv, vec3 const& n, float etai_over_etat)
{
    auto cos_theta = fmin(dot(-uv, n), 1.0);
    vec3 r_out_perp =  etai_over_etat * (uv + cos_theta*n);
    vec3 r_out_parallel = -sqrt(fabs(1.0 - r_out_perp.length_squared())) * n;
    return r_out_perp + r_out_parallel;
}

vec3 random_in_unit_disk()
{
#if VEC_RANDOM_LOOP
    for (;;)
    {
        vec3 p = vec3(random_float(-1.0f, 1.0f), random_float(-1.0f, 1.0f), 0.0f);

        if (p.length_squared() >= 1)
        {
            continue;
        }

        assert(p.length() <= 1.0f);
        return p;
    }
#else 
    vec3 p = vec3(random_float(), random_float(), 0.0f);
    p = (p / p.length()) * random_float();
    assert(p.length() <= 1.0f);
    return p;
#endif
}

struct ray
{
    vec3 origin;
    vec3 direction;

    ray()
    { }

    ray(ray const& other)
        : origin(other.origin), direction(other.direction)
    { }

    ray(vec3 const& origin, vec3 const& direction)
        : origin(origin), direction(direction)
    { }

    vec3 at(float t) const
    {
        return origin + direction * t;
    }

    void set(vec3 const& origin, vec3 const& direction)
    {
        this->origin = origin;
        this->direction = direction;
    }

    ray& operator=(ray const& other)
    {
        this->origin = other.origin;
        this->direction = other.direction;
        return* this;
    }
};

struct aabb
{
    aabb() {}
    aabb(vec3 const& min, vec3 const& max) : minimum(min), maximum(max) {}

    bool hit(ray const& r, float t_min, float t_max) const
    {
        for (int axis = 0; axis < 3; axis++)
        {
#if 1 // Andrew Kensler way
            float invD = 1.0f / r.direction[axis];
            float t0 = (minimum[axis] - r.origin[axis]) * invD;
            float t1 = (maximum[axis] - r.origin[axis]) * invD;

            if (invD < 0.0f)
            {
                std::swap(t0, t1);
            }

            t_min = t0 > t_min ? t0 : t_min;
            t_max = t1 < t_max ? t1 : t_max;

            if (t_max <= t_min)
            {
                return false;
            }
#else
            float t0 = fmin((minimum[axis] - r.origin()[axis]) / r.direction()[axis],
                    (maximum[axis] - r.origin()[axis]) / r.direction()[axis]);
            float t1 = fmax((minimum[axis] - r.origin()[axis]) / r.direction()[axis],
                    (maximum[axis] - r.origin()[axis]) / r.direction()[axis]);
            t_min = fmax(t0, t_min);
            t_max = fmin(t1, t_max);

            if (t_max <= t_min)
            {
                return false;
            }
#endif
        }

        return true;
    }

    void expand(aabb const& other)
    {
        for (int axis = 0; axis < 3; axis++)
        {
            if (other.minimum[axis] < minimum[axis])
            {
                minimum[axis] = other.minimum[axis];
            }

            if (other.maximum[axis] > maximum[axis])
            {
                maximum[axis] = other.maximum[axis];
            }
        }
    }

    // done by chat gpt
    void expand(vec3 const& v)
    {
        minimum = vec3(fmin(v.x, minimum.x), fmin(v.y, minimum.y), fmin(v.z, minimum.z));
        maximum = vec3(fmax(v.x, maximum.x), fmax(v.y, maximum.y), fmax(v.z, maximum.z));
    }


    bool is_valid() const
    {
        return !(maximum - minimum).near_zero();
    }

    vec3 minimum;
    vec3 maximum;
};

inline aabb surrounding_box(aabb const& box0, aabb const& box1)
{
     vec3 small(fmin(box0.minimum.x, box1.minimum.x),
                fmin(box0.minimum.y, box1.minimum.y),
                fmin(box0.minimum.z, box1.minimum.z));

     vec3 big(fmax(box0.maximum.x, box1.maximum.x),
              fmax(box0.maximum.y, box1.maximum.y),
              fmax(box0.maximum.z, box1.maximum.z));

    return aabb(small,big);
}

// chat gpt
class mat3 {
public:
    float m[3][3];

    mat3() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                m[i][j] = 0;
            }
        }
    }
    mat3(float m00, float m01, float m02,
            float m10, float m11, float m12,
            float m20, float m21, float m22) {
        m[0][0] = m00, m[0][1] = m01, m[0][2] = m02;
        m[1][0] = m10, m[1][1] = m11, m[1][2] = m12;
        m[2][0] = m20, m[2][1] = m21, m[2][2] = m22;
    }

    vec3 operator*(vec3 const& v) const {
        return vec3(
                m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z,
                m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z,
                m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z
                );
    }


    // operator to multiply 2 matrices together
    mat3 operator*(mat3 const& other) const {
        mat3 result;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    result.m[i][j] += m[i][k] * other.m[k][j];
                }
            }
        }
        return result;
    }
};

#endif // !MUH_MATH
