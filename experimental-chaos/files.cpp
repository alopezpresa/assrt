#ifndef MUH_FILES_CPP
#define MUH_FILES_CPP

#include <cstdio>
#include <fstream>
#include <iostream>
#include "performance.cpp"
#include "render.cpp"
#include <stb/stb_image.h>
#include <stb/stb_image_write.h>
#include <immintrin.h>
#include <vector>
#include "files.h"
#include "memory.cpp"
#include "render.h"

void write_image(char const* path, image_buffer const& buffer, image_format format)
{
    SCOPED_TIMER(write_image);

    char* image_name = new char[strlen(path) + 5];

    if (format == image_format_PPM)
    {
        std::ofstream outdata;

        sprintf(image_name, "%s%s", path, ".ppm");
        outdata.open(image_name);

        if (!outdata)
        {
            std::cerr << "Error: file could not be opened" << std::endl;
            exit(1);
        }

        outdata << "P3" << std::endl;
        outdata << buffer.width << " " << buffer.height << std::endl;
        outdata << "255" << std::endl;

        for (int j = buffer.height - 1; j >= 0; --j)
        {
            for (int i = 0; i < buffer.width; ++i)
            {
                bytes_color pixel(buffer.pixels[j*buffer.width + i], buffer.samples_per_pixel);
                outdata << (int)(pixel.r) << " "
                        << (int)(pixel.g) << " "
                        << (int)(pixel.b) << " ";
            }
            outdata << std::endl;
        }

        outdata.close();
    }
    else
    {
        int channels = 3;
        uint8_t* data = new uint8_t[buffer.height * buffer.width * channels];
        int index = 0;

        for (int j = buffer.height - 1; j >= 0; --j)
        {
            for (int i = 0; i < buffer.width; ++i)
            {
                bytes_color pixel(buffer.pixels[j*buffer.width + i], buffer.samples_per_pixel);

                for (int c = 0; c < channels; ++c)
                {
                    data[index++] = pixel.e[c];
                }
            }
        }

        if (format == image_format_PNG)
        {
            sprintf(image_name, "%s%s", path, ".png");
            stbi_write_png(image_name, buffer.width, buffer.height, channels, data, buffer.width * channels);
        }
        else if (format == image_format_JPG)
        {
            sprintf(image_name, "%s%s", path, ".jpg");
            stbi_write_jpg(image_name, buffer.width, buffer.height, channels, data, 100);
        }
        else
        {
            sprintf(image_name, "%s%s", path, ".bmp");
            stbi_write_bmp(image_name, buffer.width, buffer.height, channels, data);
        }

        delete [] data;
    }

    delete [] image_name;
}

void obj_file_count_vert_and_faces(const char* fileData, int fileSize, int* vertexCount, int* faceCount)
{
    __m256i vV = _mm256_set1_epi8('v');
    __m256i vSpace = _mm256_set1_epi8(' ');
    __m256i vF = _mm256_set1_epi8('f');

    *vertexCount = 0;
    *faceCount = 0;

    char buffer[32];
    int bytes_read;

    for (int offset = 0; offset < fileSize - 32; offset += 32)
    {
        __m256i vData = _mm256_loadu_si256((__m256i*)(fileData + offset));
        __m256i vData2 = _mm256_loadu_si256((__m256i*)(fileData + offset + 1));
        __m256i vFollowedBySpace = _mm256_cmpeq_epi8(vData2, vSpace);

        // Compare characters with 'v'
        __m256i vCompareV = _mm256_and_si256(_mm256_cmpeq_epi8(vData, vV), vFollowedBySpace);

        // Check if any 'v' characters are found
        int maskV = _mm256_movemask_epi8(vCompareV);
        int countV = _mm_popcnt_u32(maskV);
        *vertexCount += countV;
        //
        // Compare characters with 'f'
        __m256i vCompareF = _mm256_and_si256(_mm256_cmpeq_epi8(vData, vF), vFollowedBySpace);

        // Check if any 'f' characters are found
        int maskF = _mm256_movemask_epi8(vCompareF);
        int countF = _mm_popcnt_u32(maskF);
        *faceCount += countF;
    }
}

std::vector<triangle> get_obj_file_tris(const char* filepath)
{
    printf("preparing to open f: %s\n", filepath);

    std::vector<vec3> vertices;
    std::vector<triangle> triangles;

    FILE* f;
    int maxLineLenght = 255;
    char line[maxLineLenght];

    f = fopen(filepath, "r");

    if (f)
    {
        // Simple parsing, no normals or texture coordinate support
        // It expects a single object per file with no materials
        enum ParsingStep
        {
            ParsingStep_None,
            ParsingStep_ReadingVertex,
            ParsingStep_ReadingFace,
        };

        int parsingStep = ParsingStep_None;
        int lineNo = 0;

        while (fgets(line, maxLineLenght, f))
        {
            ++lineNo;
            int valuePos = 0;

            switch (line[0])
            {
                case 'v':
                    {
                        parsingStep = ParsingStep_ReadingVertex;

                        while (line[++valuePos] == ' ');
                    }
                    break;

                case 'f':
                    {
                        parsingStep = ParsingStep_ReadingFace;

                        while (line[++valuePos] == ' ');
                    }
                    break;

                case '#':
                case 'o':
                case 's':
                case '\n':
                    continue;

                default:
                    {
                        printf("bad obj read: %s, unexpected char '%c' at line %d: %s\n", filepath, line[0], lineNo, line);
                        assert(false);
                    }
            }

            switch (parsingStep)
            {
                case ParsingStep_ReadingVertex:
                    {
                        vec3 vertex;
                        int read = sscanf(line+valuePos, "%f %f %f", &vertex.x, &vertex.y, &vertex.z);
                        printf("vertex parsing: read %d float values from at line %d: %s\n", read, lineNo, line);
                        assert(read == 3);
                        vertices.push_back(vertex);
                    }
                    break;

                case ParsingStep_ReadingFace:
                    {
                        int vertexIndex1, vertexIndex2, vertexIndex3;
                        int read = sscanf(line+valuePos, "%d %d %d", &vertexIndex1, &vertexIndex2, &vertexIndex3);
                        printf("face parsing: read %d int values from at line %d: %s\n", read, lineNo, line);
                        assert(read == 3);
                        // TODO: read normals 
                        triangle t(vertices[vertexIndex1-1], vertices[vertexIndex2-1], vertices[vertexIndex3-1]);
                        triangles.push_back(t);
                    }
                    break;
            }
        }
    }
    else
    {
        printf("Failed to open f: %s\n", filepath);
    }

    fclose(f);

    return triangles;
}

#endif // !MUH_FILES_CPP
