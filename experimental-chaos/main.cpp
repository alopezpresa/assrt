#include <cstring>
#define SCENE_EMPTY  0
#define SCENE_SIMPLE 1
#define SCENE_EASY   2
#define SCENE_HARD   3
#define SCENE_MESH_SIMPLE   4
#define SCENE_MESH_FILES   5
#define SCENE SCENE_MESH_FILES // <---- EDIT THIS!

#define USE_SDL 1 // <---- EDIT THIS!
#define DUMB_LOOP 0 // <---- EDIT THIS!
#define TEST_FRAME_COUNT 0 // <---- EDIT THIS!

#define PERF_TIMERS 0 // this is not ok, 300ms render gave a 0.050s frame time...

#define RANDOM_SAMPLES 1
#define FIXED_SAMPLES 2
#define SINGLE_SAMPLE 3
#define SINGLE_SAMPLE_FOR_SIMD_RAYS 4
#define MULTI_SAMPLES_FOR_SIMD_RAYS 5


#define SIMD_FLOAT_WIDTH 8 // no need to change this unless we want to support non avx simd

#define PROCESS_RAYS_WITH_SIMD 1 // <---- EDIT THIS!

#if PROCESS_RAYS_WITH_SIMD
#define PROCESS_MESHES_WITH_SIMD 0
#define SAMPLING_METHOD SINGLE_SAMPLE_FOR_SIMD_RAYS // <---- EDIT THIS!
#else
#define PROCESS_MESHES_WITH_SIMD 0 // <---- EDIT THIS!
#define SAMPLING_METHOD SINGLE_SAMPLE // <---- EDIT THIS!
#endif

#if PROCESS_MESHES_WITH_SIMD
#define PER_FRAME_SIMD_TRANSFORM 0 // <---- EDIT THIS!
#define NASTY_CPP_SIMD_THAT_HIDES_WHAT_HAPPENS 0 // <---- EDIT THIS!
#else
#define PER_FRAME_SIMD_TRANSFORM 0
#define NASTY_CPP_SIMD_THAT_HIDES_WHAT_HAPPENS 0
#endif

#define THREAD_COUNT 8 // <---- EDIT THIS!

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory>
#include <thread>
#include <cassert>

int window_width = 1280;
int window_height = 720;
int target_framerate = 600; // lolz
int raytrace_downscale = 2;
int max_depth = 8;

#if SAMPLING_METHOD == MULTI_SAMPLES_FOR_SIMD_RAYS
    int samples_per_pixel = SIMD_FLOAT_WIDTH/2;
#elif SAMPLING_METHOD == SINGLE_SAMPLE_FOR_SIMD_RAYS
    int samples_per_pixel = 1;
#elif SAMPLING_METHOD == RANDOM_SAMPLES
    int samples_per_pixel = 4;
#elif SAMPLING_METHOD == FIXED_SAMPLES
    int samples_per_pixel = 3;
    float sample_offset_x[] = {-0.00f, -0.13f,  0.13f, 0.13f};
    float sample_offset_y[] = { 0.13f, -0.13f, -0.13f, 0.13f};
#elif SAMPLING_METHOD == SINGLE_SAMPLE
    int samples_per_pixel = 1;
#endif

#define CONCAT(a, b) CONCAT_INNER(a, b)
#define CONCAT_INNER(a, b) a ## b

#include "memory.cpp"
#include "performance.cpp"
#include "math.cpp"
#include "render.cpp"
#include "scenes.cpp"
#include "files.cpp"

#if USE_SDL
#include <SDL2/SDL.h>
#endif

struct scene_config
{
    int max_depth;
    image_buffer* image_data;
    camera* cam;
    RenderObjectID world;
};

enum pixel_job_status
{
    pixel_job_status_todo,
    pixel_job_status_wip,
    pixel_job_status_done,
};


#if PROCESS_RAYS_WITH_SIMD
struct multi_pixel_job
{
    multi_pixel_job() : status(pixel_job_status_done) { }
    std::atomic<int> status;
    int xs[SIMD_FLOAT_WIDTH];
    int ys[SIMD_FLOAT_WIDTH];
};

multi_pixel_job pending_jobs[THREAD_COUNT];
#endif

#if PROCESS_RAYS_WITH_SIMD
void queue_multi_pixel_job(int* xs, int* ys)
{
    bool queued = false;

    while (!queued)
    {
        for (int t = 0; t < THREAD_COUNT; ++t)
        {
            if (pending_jobs[t].status == pixel_job_status_done)
            {
                memcpy(pending_jobs[t].xs, xs, SIMD_FLOAT_WIDTH*sizeof(int));
                memcpy(pending_jobs[t].ys, ys, SIMD_FLOAT_WIDTH*sizeof(int));
                pending_jobs[t].status = pixel_job_status_todo;

                queued = true;

                break;
            }
        }
    }
}
#else
struct pixel_job
{
    pixel_job() : status(pixel_job_status_done) { }
    std::atomic<int> status;
    int x, y;
};

pixel_job pending_jobs[THREAD_COUNT];

void queue_pixel_job(int x, int y)
{
    bool queued = false;

    while (!queued)
    {
        for (int t = 0; t < THREAD_COUNT; ++t)
        {
            if (pending_jobs[t].status == pixel_job_status_done)
            {
                pending_jobs[t].x = x;
                pending_jobs[t].y = y;
                pending_jobs[t].status = pixel_job_status_todo;

                queued = true;

                break;
            }
        }
    }
}
#endif

std::unique_ptr<std::thread> threads_pool[THREAD_COUNT];
bool process_jobs;

void wait_all_pixel_jobs()
{
    bool active_jobs = true;

    while (!active_jobs)
    {
        active_jobs = false;

        for (int t = 0; t < THREAD_COUNT; ++t)
        {
            if (pending_jobs[t].status != pixel_job_status_done)
            {
                active_jobs = true;
                break;
            }
        }
    }
}

#if PROCESS_RAYS_WITH_SIMD
void multi_pixel_worker(scene_config* config, int job_index)
{
    multi_pixel_job* assigned_job = &pending_jobs[job_index];

    while (process_jobs)
    {
        if (assigned_job->status == pixel_job_status_todo)
        {
            assigned_job->status = pixel_job_status_wip;

            color colors[SIMD_FLOAT_WIDTH];
            ray rays[SIMD_FLOAT_WIDTH];

            for (int i = 0; i < SIMD_FLOAT_WIDTH; ++i)
            {
#if SAMPLING_METHOD == SINGLE_SAMPLE_FOR_SIMD_RAYS
                float u = float(assigned_job->xs[i]) / float(config->image_data->width-1);
                float v = float(assigned_job->ys[i]) / float(config->image_data->height-1);
#elif SAMPLING_METHOD == MULTI_SAMPLES_FOR_SIMD_RAYS
                float u = float(assigned_job->xs[i] + (random_float() - 0.5f)) / float(config->image_data->width-1);
                float v = float(assigned_job->ys[i] + (random_float() - 0.5f)) / float(config->image_data->height-1);
#else
            BAD_SETTINGS!
#endif
                rays[i] = config->cam->get_ray(u, v);

                int pixel_index = assigned_job->ys[i] * config->image_data->width + assigned_job->xs[i];
                config->image_data->pixels[pixel_index] = color::zero();
            }

            simd_ray_color(rays, colors, config->world, config->max_depth);

            for (int i = 0; i < SIMD_FLOAT_WIDTH; ++i)
            {
                int pixel_index = assigned_job->ys[i] * config->image_data->width + assigned_job->xs[i];
                config->image_data->pixels[pixel_index] += colors[i];
            }

            assigned_job->status = pixel_job_status_done;
        }
    }
}
#else
void pixel_worker(scene_config* config, int job_index)
{
    pixel_job* assigned_job = &pending_jobs[job_index];

    while (process_jobs)
    {
        if (assigned_job->status == pixel_job_status_todo)
        {
            assigned_job->status = pixel_job_status_wip;

            color c = color::zero();

            for (int s = 0; s < config->image_data->samples_per_pixel; ++s)
            {
#if SAMPLING_METHOD == RANDOM_SAMPLES
                float offset_x = random_float();
                float offset_y = random_float();
#elif SAMPLING_METHOD == FIXED_SAMPLES
                float offset_x = sample_offset_x[s];
                float offset_y = sample_offset_y[s];
#elif SAMPLING_METHOD == SINGLE_SAMPLE
                s = config->image_data->samples_per_pixel;
                constexpr float offset_x = 0.0f;
                constexpr float offset_y = 0.0f;
#endif
                float u = float(assigned_job->x + offset_x ) / float(config->image_data->width-1);
                float v = float(assigned_job->y + offset_y ) / float(config->image_data->height-1);
                ray r = config->cam->get_ray(u, v);
                c += ray_color(r, config->world, config->max_depth);
            }

            int pixel_index = assigned_job->y * config->image_data->width + assigned_job->x;
            config->image_data->pixels[pixel_index] = c;
            assigned_job->status = pixel_job_status_done;
        }
    }
}
#endif

void init_thread_pool(scene_config* config)
{
    process_jobs = true;

    for (int i = 0; i < THREAD_COUNT; ++i)
    {
#if PROCESS_RAYS_WITH_SIMD
        threads_pool[i] = std::make_unique<std::thread>(multi_pixel_worker, config, i);
#else
        threads_pool[i] = std::make_unique<std::thread>(pixel_worker, config, i);
#endif
    }
}

void stop_thread_pool()
{
    process_jobs = false;

    for (int i = 0; i < THREAD_COUNT; ++i)
    {
        threads_pool[i]->join();
        threads_pool[i] = nullptr;
    }
}

void raytrace_frame(image_buffer* image_data)
{
    SCOPED_TIMER(raytrace_image);

#if PROCESS_RAYS_WITH_SIMD
    int queued_pixels = 0;
    int xs[SIMD_FLOAT_WIDTH];
    int ys[SIMD_FLOAT_WIDTH];

    for (int pixel_index = 0; pixel_index < image_data->height * image_data->width; ++pixel_index)
    {
        int x = pixel_index % image_data->width;
        int y = pixel_index / image_data->width;

        for (int i = 0; i < image_data->samples_per_pixel; ++i)
        {
            xs[queued_pixels] = x;
            ys[queued_pixels] = y;
            ++queued_pixels;
        }

        if (queued_pixels == SIMD_FLOAT_WIDTH)
        {
            queue_multi_pixel_job(xs, ys);
            queued_pixels = 0;
        }
    }

    if (queued_pixels != 0)
    {
        queue_multi_pixel_job(xs, ys);
    }
#else
    for (int y = image_data->height-1; y >= 0; --y)
    {
        for (int x = 0; x < image_data->width; ++x)
        {
            queue_pixel_job(x, y);
        }
    }
#endif

    wait_all_pixel_jobs();
}


#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
#else
int main (int argc, char* argv[])
{
    for (int i = 1; i < argc; ++i)
    {
        if (strcmp(argv[i], "-w") == 0 && argc > i + 1 )
        {
            window_width = (int)strtol(argv[++i], NULL, 10);
        }
        else if (strcmp( argv[i], "-h") == 0 && argc > i + 1 )
        {
            window_height = (int)strtol(argv[++i], NULL, 10);
        }
        else if (strcmp( argv[i], "-fps") == 0 && argc > i + 1)
        {
            target_framerate = (int)strtol(argv[++i], NULL, 10);
        }
    }
#endif

    int frametime_target = TIMER_PRECISION / target_framerate;
    float aspect_ratio = (float)window_width/(float)window_height;

    camera cam;

    RenderObjectID world = build_scene(&cam, aspect_ratio);

    image_buffer image_data(window_width/raytrace_downscale, window_height/raytrace_downscale, samples_per_pixel);

    scene_config config = 
    {
        max_depth,
        &image_data,
        &cam,
        world
    };

    init_thread_pool(&config);

#if PROCESS_MESHES_WITH_SIMD
    pre_alloc_for_meshes_into_wide_data();
#if !PER_FRAME_SIMD_TRANSFORM
    preprocess_meshes_into_wide_data();
#endif
#endif

#if USE_SDL
    /* There are no error checks for brevity.  If something goes wrong, it's easy to add a check and a call to SDL_GetError() to figure out why.  */ /* We initialize the SDL video and event subsystems.  If we forget or leave ot the SDL_INIT_EVENTS, the SDL_CreateWindow() will initialize it for us.  There is no error checking in our example code, and in a real game we might explicitly check if this fails and try to notify the user somehow.  */ SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);

    /*
       We create a new window to display our message.  Here we pass in *
       the title text, where it should be on the screen (which is *
       undefined) and the size as width and height, and finally a flag to say *
       it's shown.
     */
    SDL_Window *window = SDL_CreateWindow("A Simple and Slow Raytracer | ASS RT", 
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, 
            window_width, window_height, SDL_WINDOW_SHOWN);

    SDL_SetWindowResizable(window, SDL_TRUE);

    SDL_Surface *surface = SDL_LoadBMP( "hello.bmp" );

    /*
       We create a hardware accelerated renderer.  This is liable to
       fail on some systems, and again there's no error checking.

       We pass in the window we created earlier, and use -1 for the
       rendering driver to get the first one available.

       We can explicitly ask for hardware redering with SDL_RENDERER_ACCELERATED.
     */
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.
    SDL_RenderSetLogicalSize(renderer, image_data.width, image_data.height);

    // We create the texture we want to disaply from the surface we loaded earlier.
    SDL_Texture* bmp_texture = SDL_CreateTextureFromSurface(renderer, surface);

    SDL_Texture* sdlTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, image_data.width, image_data.height);

    // We're now done with the surface, so we free the resources.
    // And set the pointer to NULL so it won't accidentally be used for something else.
    SDL_FreeSurface(surface);
    surface = NULL;

    SDL_bool quit = SDL_FALSE;
    SDL_Event e = { 0 };

    float dt = 0.0f;

    int channels = 4;
    uint8_t* data = new uint8_t[image_data.height * image_data.width * channels];

    /*
       Here is a very simple game loop, with a soft framerate.  There is
       a chance an individual frame will take longer, and this happens
       predictably when the user presses the X button to close the
       window, and the delay routine can oversleep because of operating
       system scheduling.
     */
    while (!quit)
    {
        Uint32 start = SDL_GetTicks();

        while (SDL_PollEvent(&e))
        {
            switch (e.type)
            {
                case SDL_QUIT:
                    printf("last dt: %.3f\n", dt);
                    quit = SDL_TRUE;
                    break;
                default:
                    break;
            }
        }

        const uint8_t* kb = SDL_GetKeyboardState(NULL);

        float walk_sprint_factor = 3;
        float walk_speed = 2;

        float rotate_sprint_factor = 2;
        float rotate_speed = 50;

        if (kb[SDL_SCANCODE_LSHIFT])
        {
            walk_speed *= walk_speed;
            rotate_speed *= rotate_sprint_factor;
        }

        vec3 delta_cam_pos(0, 0, 0);
        delta_cam_pos.x = kb[SDL_SCANCODE_C] - kb[SDL_SCANCODE_Z];
        delta_cam_pos.z = kb[SDL_SCANCODE_S] - kb[SDL_SCANCODE_W];

        float delta_cam_pos_len = delta_cam_pos.length();

        if (delta_cam_pos_len > 1.0f)
        {
            delta_cam_pos /= delta_cam_pos_len;
        }

        cam.move(delta_cam_pos * dt * walk_speed);

        vec3 delta_cam_rot(0, 0, 0);
        delta_cam_rot.x = kb[SDL_SCANCODE_DOWN] - kb[SDL_SCANCODE_UP];
        delta_cam_rot.y = kb[SDL_SCANCODE_A] - kb[SDL_SCANCODE_D];
        delta_cam_rot.z = kb[SDL_SCANCODE_LEFT] - kb[SDL_SCANCODE_RIGHT];

        float delta_cam_target_len = delta_cam_rot.length();

        if (delta_cam_target_len > 1.0f)
        {
            delta_cam_rot /= delta_cam_target_len;
        }

        cam.rotate(delta_cam_rot * dt * rotate_speed);

        cam.update_viewport();

#if PER_FRAME_SIMD_TRANSFORM
        preprocess_meshes_into_wide_data();
#endif
        raytrace_frame(&image_data);

        // We clear the screen with the default colour (because we don't explicitly set the colour anywhere).
        SDL_RenderClear(renderer);

        /*
           Then copy the texture we created on to the entire screen.
           That's what the NULL, NULL means: use the entire texture on the entire screen.
         */
        /*
           SDL_RenderCopy(renderer, bmp_texture, NULL, NULL );
         */

        {
            SCOPED_TIMER(float_to_byte);
            static __m256 m256_inv_samples = _mm256_set1_ps(1.0f / image_data.samples_per_pixel);
            static __m256 m256_zero = _mm256_set1_ps(0.0f);
            static __m256 m256_0999 = _mm256_set1_ps(0.999f);
            static __m256 m256_256 = _mm256_set1_ps(256);
            int index = 0;

            for (int y = image_data.height - 1; y >= 0; --y)
            {
                for (int x = 0; x < image_data.width; x+=2)
                {
                    __m256 color_channels = _mm256_load_ps((float*)&image_data.pixels[y*image_data.width + x]);
                    color_channels = _mm256_mul_ps(color_channels, m256_inv_samples);
                    color_channels = _mm256_sqrt_ps(color_channels);
                    color_channels = _mm256_min_ps(_mm256_max_ps(color_channels, m256_zero), m256_0999);
                    color_channels = _mm256_mul_ps(color_channels, m256_256);

                    data[index++] = (uint8_t)color_channels[2]; // b
                    data[index++] = (uint8_t)color_channels[1]; // g
                    data[index++] = (uint8_t)color_channels[0]; // r
                    data[index++] = (uint8_t)color_channels[3]; // a
                    data[index++] = (uint8_t)color_channels[6]; // b
                    data[index++] = (uint8_t)color_channels[5]; // g
                    data[index++] = (uint8_t)color_channels[4]; // r
                    data[index++] = (uint8_t)color_channels[7]; // a
                }
            }
        }

        SDL_UpdateTexture(sdlTexture, NULL, data, image_data.width * channels * sizeof(uint8_t));

        SDL_RenderCopy(renderer, sdlTexture, NULL, NULL);

        // And finally tell the renderer to display on screen what we've drawn so far.
        SDL_RenderPresent(renderer);

        Uint32 end = SDL_GetTicks();

        int frame_ms = end - start;
        int wait_for = frametime_target - frame_ms;

        if (wait_for > 0)
        {
            SDL_Delay(wait_for);
        }

        dt = (float)(SDL_GetTicks() - start) / (float)TIMER_PRECISION;

        static float min_dt = 0;
        static float max_dt = 0;
        static int skip_frames = 15;

        if (skip_frames > 0)
        {
            --skip_frames;
        }
        else if (skip_frames == 0)
        {
            --skip_frames;
            min_dt = dt;
            max_dt = dt;
        }
        else
        {
            if (min_dt > dt) min_dt = dt;
            if (max_dt < dt) max_dt = dt;
            printf("> dt: %.3f | min: %.3f | max: %.3f | fps: %.2f\n", dt, min_dt, max_dt, 1.0f/dt);

#if TEST_FRAME_COUNT
            static int test_frames = TEST_FRAME_COUNT;

            if (--test_frames == 0)
            {
                quit = SDL_TRUE;
            }
#endif
        }

    }

    delete [] data;

    // Clean up after ourselves before we quit.
    SDL_DestroyTexture(bmp_texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
#elif DUMB_LOOP
    for (;;)
    {
        raytrace_frame(&image_data);
    }
#else
    raytrace_frame(&image_data);
    write_image("image", image_data, image_format_BMP);
#endif

    stop_thread_pool();

    printf("\n");

    return 0;
}

