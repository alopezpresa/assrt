#ifndef MUH_RENDER_H
#define MUH_RENDER_H

#include <math.h>
#include "math.cpp"

typedef int MaterialID;
typedef int RenderObjectID;

inline float reflectance(float cosine, float ref_idx)
{
    // Use Schlick's approximation for reflectance.
    float r0 = (1.0f-ref_idx) / (1.0f+ref_idx);
    r0 = r0*r0;
    return r0 + (1.0f-r0) * pow((1.0f - cosine), 5.0f);
}


union color4
{
    struct
    {
        float r;
        float g;
        float b;
        float a;
    };

    float e[4];

    color4()
    {
    }

    color4(float r, float g, float b, float a)
    : r(r), g(g), b(b), a(a)
    { }

    color4(color const& c)
    {
        r = c.r;
        g = c.g;
        b = c.b;
        a = 1.0f;
    }

    inline color4& operator+=(color4 const& b)
    {
        this->r += b.r;
        this->g += b.g;
        this->b += b.b;
        this->a += b.a;
        return *this;
    }
};

struct image_buffer
{
    image_buffer(int width, int height, int samples_per_pixel)
    {
        this->width = width;
        this->height = height;
        this->samples_per_pixel = samples_per_pixel;

        pixels = new color4[height * width];
    }

    int width;
    int height;
    int samples_per_pixel;
    color4* pixels;
};

union bytes_color
{
    struct
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    };

    uint8_t e[3];

    bytes_color(color4 color, int samples_per_pixel)
    {
        // Divide the color by the number of samples and gamma-correct for gamma=2.0.
        float scale = 1.0 / samples_per_pixel;
        r = (uint8_t)(clamp(sqrt(scale * color.r), 0.0f, 0.999f) * 256);
        g = (uint8_t)(clamp(sqrt(scale * color.g), 0.0f, 0.999f) * 256);
        b = (uint8_t)(clamp(sqrt(scale * color.b), 0.0f, 0.999f) * 256);
    }

    bytes_color(color color, int samples_per_pixel)
    {
        // Divide the color by the number of samples and gamma-correct for gamma=2.0.
        float scale = 1.0 / samples_per_pixel;
        r = (uint8_t)(clamp(sqrt(scale * color.r), 0.0f, 0.999f) * 256);
        g = (uint8_t)(clamp(sqrt(scale * color.g), 0.0f, 0.999f) * 256);
        b = (uint8_t)(clamp(sqrt(scale * color.b), 0.0f, 0.999f) * 256);
    }
};

struct hit_record
{
    vec3 p;
    vec3 normal;
    float t;
    bool front_face;
    MaterialID material_id;

    hit_record() : t(-1), front_face(false)
    { }

    void set_face_normal(ray r, vec3 outward_normal)
    {
        front_face = dot(r.direction, outward_normal) < 0;
        normal = front_face ? outward_normal : -outward_normal;
    }
};

enum MaterialType
{
    MaterialType_Lambertian,
    MaterialType_Metal,
    MaterialType_Dielectric
};

struct Material
{
    MaterialType type;
    color albedo;

    union
    {
        float fuzz;
        float ir; // Index of Refraction
    };

    Material(MaterialType type) : type(type) { }

    bool scatter(ray const& r_in, hit_record const& hit, color* attenuation, ray* scattered) const
    {
        switch (type)
        {
            case MaterialType_Lambertian:
                {
                    // NOTE:
                    // Other scattering tecnics for mate lambertian materials:
                    // vec3 target = hit.p + random_in_hemisphere(hit.normal);
                    // using random_in_hemisphere or random_in_unit_sphere will work, but 
                    // it's not a proper aproximation and results in darker, less acurate results
                    // It should be faster however because we're saving lenght and other
                    // calculations.

                    vec3 scatter_direction = hit.normal + random_unit_vector();

                    if (scatter_direction.near_zero())
                    {
                        scatter_direction = hit.normal;
                    }

                    scattered->set(hit.p, scatter_direction);
                    *attenuation = albedo;

                    return true;
                }
                break;

            case MaterialType_Metal:
                {
                    vec3 reflected = reflect(unit_vector(r_in.direction), hit.normal);
                    scattered->set(hit.p, reflected + fuzz*random_in_unit_sphere());
                    *attenuation = albedo;
                    return (dot(scattered->direction, hit.normal) > 0);
                }
                break;

            case MaterialType_Dielectric:
                {
                    *attenuation = color(1.0f, 1.0f, 1.0f);

                    float refraction_ratio = hit.front_face ? (1.0f/ir) : ir;

                    vec3 unit_direction = unit_vector(r_in.direction);

                    float cos_theta = fmin(dot(-unit_direction, hit.normal), 1.0f);
                    float sin_theta = sqrt(1.0f - cos_theta*cos_theta);

                    bool cannot_refract = refraction_ratio * sin_theta > 1.0f;
                    vec3 direction;

                    if (cannot_refract || reflectance(cos_theta, refraction_ratio) > random_float())
                    {
                        direction = reflect(unit_direction, hit.normal);
                    }
                    else
                    {
                        direction = refract(unit_direction, hit.normal, refraction_ratio);
                    }

                    scattered->set(hit.p, direction);

                    return true;
                }
                break;
        }
    }
};

struct triangle
{
    vec3 a, b, c, n;

    triangle(vec3 a, vec3 b, vec3 c)
    : a(a), b(b), c(c)
    {
        n = cross(b - a, c - a);
        n.normalize();
    }

    bool hit(ray const& r, float t_min, float t_max, hit_record* result) const
    {
        vec3 v0v1 = b - a;
        vec3 v0v2 = c - a;
        vec3 pvec = cross(r.direction, v0v2);
        float det = dot(v0v1, pvec);
        if (det < epsilon) return false;

        float invDet = 1 / det;
        vec3 tvec = r.origin - a;
        float u = dot(tvec, pvec) * invDet;
        if (u < 0 || u > 1) return false;

        vec3 qvec = cross(tvec, v0v1);
        float v = dot(r.direction, qvec) * invDet;
        if (v < 0 || u + v > 1) return false;

        float t = dot(v0v2, qvec) * invDet;
        if (t < t_min || t > t_max) return false;

        result->t = t;
        result->p = r.at(t);
        result->normal = cross(v0v1, v0v2).normal();
        return true;
    }
};

enum RenderObjectType
{
    RenderObjectType_BVHNode,
    RenderObjectType_List,
    RenderObjectType_Sphere,
    RenderObjectType_Mesh
};

struct RenderObject
{
    RenderObject() : type((RenderObjectType)-1){};

    RenderObjectType type;
    MaterialID material_id;
    aabb bounding_box;

    union 
    {
        struct
        {
            vec3 center;
            float radius;
        } sphere_data;

        struct
        {
            RenderObjectID left_id;
            RenderObjectID right_id;
        } node_data;
    };

    struct
    {
        std::vector<triangle> triangles;
    } mesh_data;

    struct
    {
        std::vector<RenderObjectID> objects;
    } list_data;
};

MaterialID Material_CreateLambertian(color const& a);
MaterialID Material_CreateMetal(color const& a, float f);
MaterialID Material_CreateDielectric(float index_of_refraction);
Material* Material_Get(MaterialID id);


RenderObject* RenderObject_Get(RenderObjectID id);
RenderObjectID RenderObject_CreateSphere(vec3 center, float radius, MaterialID material_id);
RenderObjectID RenderObject_CreateMesh(std::vector<triangle> const& triangles, MaterialID material_id);

#endif // !MUH_RENDER_H
