#ifndef MUH_RENDER_CPP
#define MUH_RENDER_CPP

#include <ctype.h>
#include <cstdio>
#include <immintrin.h>
#include <math.h>
#include <smmintrin.h>
#include <xmmintrin.h>
#include "math.cpp"
#include "wmath.cpp"
#include "render.h"

std::vector<Material> materials;

Material* Material_Get(MaterialID id)
{
    assert(0 <= id && id < materials.size());
    return &materials[id];
}

MaterialID Material_Create(MaterialType type)
{
    Material new_material(type);
    materials.push_back(new_material);
    MaterialID new_material_id = materials.size() - 1;
    return new_material_id;
}

MaterialID Material_CreateLambertian(color const& a)
{
    MaterialID new_material_id = Material_Create(MaterialType_Lambertian);

    materials[new_material_id].albedo = a;

    return new_material_id;
}

MaterialID Material_CreateMetal(color const& a, float f)
{
    MaterialID new_material_id = Material_Create(MaterialType_Metal);

    materials[new_material_id].albedo = a;
    materials[new_material_id].fuzz = clamp(f, 0, 1);

    return new_material_id;
}

MaterialID Material_CreateDielectric(float index_of_refraction)
{
    MaterialID new_material_id = Material_Create(MaterialType_Dielectric);

    materials[new_material_id].ir = index_of_refraction;

    return new_material_id;
}

std::vector<RenderObject> render_objects;

RenderObject* RenderObject_Get(RenderObjectID id)
{
    return &render_objects[id];
}

RenderObjectID RenderObject_Create(RenderObjectType type, MaterialID material_id)
{
    RenderObject new_object;
    new_object.type = type;
    new_object.material_id = material_id;
    render_objects.push_back(new_object);
    return render_objects.size()-1;
}

RenderObjectID RenderObject_CreateSphere(vec3 center, float radius, MaterialID material_id)
{
    RenderObjectID sphere_id = RenderObject_Create(RenderObjectType_Sphere, material_id);
    RenderObject* sphere = RenderObject_Get(sphere_id);

    sphere->sphere_data.center = center;
    sphere->sphere_data.radius = radius;

    vec3 radius_delta(radius, radius, radius);
    sphere->bounding_box.minimum = center - radius_delta;
    sphere->bounding_box.maximum = center + radius_delta;

    return sphere_id;
}

RenderObjectID RenderObject_CreateMesh(std::vector<triangle> const& triangles, MaterialID material_id)
{
    RenderObjectID mesh_id = RenderObject_Create(RenderObjectType_Mesh, material_id);
    RenderObject* mesh = RenderObject_Get(mesh_id);

    mesh->mesh_data.triangles = triangles;
    for (triangle const& t : triangles)
    {
        mesh->bounding_box.expand(t.a);
        mesh->bounding_box.expand(t.b);
        mesh->bounding_box.expand(t.c);
    }

    return mesh_id;
}

inline bool box_compare(RenderObjectID a, RenderObjectID b, int axis) {
    aabb const& box_a = RenderObject_Get(a)->bounding_box;
    aabb const& box_b = RenderObject_Get(b)->bounding_box;

    if (!box_a.is_valid() || !box_b.is_valid())
    {
        std::cerr << "box_compare error. No bounding box in bvh_node constructor.\n";
    }

    return box_a.minimum.e[axis] < box_b.minimum.e[axis];
}

inline bool box_x_compare (RenderObjectID a, RenderObjectID b) {
    return box_compare(a, b, 0);
}

inline bool box_y_compare (RenderObjectID a, RenderObjectID b) {
    return box_compare(a, b, 1);
}

inline bool box_z_compare (RenderObjectID a, RenderObjectID b) {
    return box_compare(a, b, 2);
}

RenderObjectID RenderObject_CreateBVHNode(std::vector<RenderObjectID> const& objects)
{
    RenderObjectID node_id = RenderObject_Create(RenderObjectType_BVHNode, -1);

    int axis = random_int(0,3);
    auto comparator = (axis == 0) ? box_x_compare
                    : ((axis == 1) ? box_y_compare : box_z_compare);

    if (objects.size() == 1)
    {
        RenderObject* node = RenderObject_Get(node_id);
        node->node_data.left_id = node->node_data.right_id = objects[0];
    }
    else if (objects.size() == 2)
    {
        RenderObject* node = RenderObject_Get(node_id);
        if (comparator(objects[0], objects[1]))
        {
            node->node_data.left_id = objects[0];
            node->node_data.right_id = objects[1];
        }
        else
        {
            node->node_data.left_id = objects[1];
            node->node_data.right_id = objects[0];
        }
    }
    else
    {
        std::vector<RenderObjectID> objects_copy = objects;
        std::sort(objects_copy.begin(), objects_copy.end(), comparator);
        auto mid = objects_copy.begin() + objects_copy.size()/2;

        std::vector<RenderObjectID> first_half(objects_copy.begin(), mid);
        RenderObject_Get(node_id)->node_data.left_id = RenderObject_CreateBVHNode(first_half);

        std::vector<RenderObjectID> second_half(mid, objects_copy.end());
        RenderObject_Get(node_id)->node_data.right_id = RenderObject_CreateBVHNode(second_half);
    }

    aabb const& box_left = RenderObject_Get(RenderObject_Get(node_id)->node_data.left_id)->bounding_box;
    aabb const& box_right = RenderObject_Get(RenderObject_Get(node_id)->node_data.right_id)->bounding_box;

    if (!box_left.is_valid() || !box_right.is_valid())
    {
        std::cerr << "RenderObject_CreateNode - No bounding box in bvh_node constructor.\n";
    }

    RenderObject_Get(node_id)->bounding_box = surrounding_box(box_left, box_right);
    return node_id;
}

RenderObjectID RenderObject_CreateList(std::vector<RenderObjectID> const& objects)
{
    RenderObjectID list_id = RenderObject_Create(RenderObjectType_List, -1);
    RenderObject* list = RenderObject_Get(list_id);
    list->list_data.objects = objects;
    
    for (RenderObjectID otherID : objects)
    {
        RenderObject *other = RenderObject_Get(otherID);
        list->bounding_box.expand(other->bounding_box);
    }

    return list_id;
}


bool RenderObject_HitRay(RenderObjectID object_id, ray const& r, float t_min, float t_max, hit_record* result)
{
    RenderObject* object = RenderObject_Get(object_id);

    if (!object->bounding_box.hit(r, t_min, t_max))
    {
        return false;
    }

    switch (object->type)
    {
        case RenderObjectType_BVHNode:
            {
                bool did_hit_left = RenderObject_HitRay(object->node_data.left_id, r, t_min, t_max, result);
                if (object->node_data.left_id != object->node_data.right_id)
                {
                    bool did_hit_right = RenderObject_HitRay(object->node_data.right_id, r, t_min, did_hit_left? result->t : t_max, result);
                    return did_hit_left || did_hit_right;
                }
                else
                {
                    return did_hit_left;
                }
            }
            break;
        case RenderObjectType_List:
            {
                hit_record best_hit;
                for (RenderObjectID id : object->list_data.objects)
                {
                    hit_record current_hit;
                    if (RenderObject_HitRay(id, r, t_min, t_max, &current_hit))
                    {
                        if (best_hit.t < 0 || best_hit.t > current_hit.t)
                        {
                            best_hit = current_hit;
                        }
                    }
                }

                *result = best_hit;

                return result->t > 0;
            }
            break;
        case RenderObjectType_Sphere:
            {
                vec3 oc = r.origin - object->sphere_data.center;
                float a = r.direction.length_squared();
                float half_b = dot(oc, r.direction);
                float c = oc.length_squared() - object->sphere_data.radius*object->sphere_data.radius;

                float discriminant = half_b*half_b - a*c;

                if (discriminant < 0)
                {
                    return false;
                }

                float sqrtd = sqrt(discriminant);

                // Find the nearest root that lies in the acceptable range.
                float root = (-half_b - sqrtd) / a;
                if (root < t_min or t_max < root)
                {
                    root = (-half_b + sqrtd) / a;

                    if (root < t_min or t_max < root)
                    {
                        return false;
                    }
                }

                result->t = root;
                result->p = r.at(result->t);
                vec3 outward_normal = (result->p - object->sphere_data.center) / object->sphere_data.radius;
                result->set_face_normal(r, outward_normal);
                result->material_id = object->material_id;

                return true;
            }
            break;
        case RenderObjectType_Mesh:
            {
#if BRANCHLESS_CODE_THAT_ENDED_UP_BEING_SLOWER
                hit_record options[2];
                options[0] = *result;
                options[1].material_id = object->material_id;

                bool hit_something = false;

                for (triangle const& tri : object->mesh_data.triangles)
                {
                    vec3 v0v1 = tri.b - tri.a;
                    vec3 v0v2 = tri.c - tri.a;
                    vec3 pvec = cross(r.direction, v0v2);
                    float det = dot(v0v1, pvec);
                    bool did_hit = !(det < epsilon);

                    float invDet = 1 / det;
                    vec3 tvec = r.origin - tri.a;
                    float u = dot(tvec, pvec) * invDet;
                    did_hit &= !(u < 0 || u > 1);

                    vec3 qvec = cross(tvec, v0v1);
                    float v = dot(r.direction, qvec) * invDet;
                    did_hit &= !(v < 0 || u + v > 1);

                    float t = dot(v0v2, qvec) * invDet;
                    did_hit &= !(t < t_min || t > t_max);

                    options[1].t = t;
                    options[1].p = r.at(t);
                    options[1].normal = cross(v0v1, v0v2).normal();

                    bool is_better = (options[1].t > 0 && options[1].t < options[0].t) || options[0].t < 0;
                    bool best_tri_so_far = did_hit && is_better;
                    options[0] = options[best_tri_so_far];
                }

                *result = options[0];
                return result->t >= 0;
#else
                for (triangle const& t : object->mesh_data.triangles)
                {
                    hit_record temp_result;
                    if (t.hit(r, t_min, t_max, &temp_result))
                    {
                        if ((temp_result.t > 0 && temp_result.t < result->t) || result->t < 0)
                        {
                            *result = temp_result;
                            result->material_id = object->material_id;
                        }
                    }
                }

                return result->t >= 0;
#endif
            }
            break;
    }
}

#if PROCESS_MESHES_WITH_SIMD
int triangles_count = 0;

#if NASTY_CPP_SIMD_THAT_HIDES_WHAT_HAPPENS

struct wide_triangles
{
    w_vec3 a, b, c, n;
    __m256 material_ids;

    wide_triangles() {};
};

static int wide_tris_count = 0;
static wide_triangles* world_triangles_packed = NULL;

#define SET_WVEC3_FIELD(wbox_ptr, box, sub_idx, field)\
    wbox_ptr->field.x[sub_idx] = box.field.x;\
    wbox_ptr->field.y[sub_idx] = box.field.y;\
    wbox_ptr->field.z[sub_idx] = box.field.z

#else
int total_tri_buffer_size;

static float* tri_a_x = NULL;
static float* tri_a_y = NULL;
static float* tri_a_z = NULL;
static float* tri_b_x = NULL;
static float* tri_b_y = NULL;
static float* tri_b_z = NULL;
static float* tri_c_x = NULL;
static float* tri_c_y = NULL;
static float* tri_c_z = NULL;
static float* tri_material_id = NULL;

#endif

#endif

void pre_alloc_for_meshes_into_wide_data()
{
#if PROCESS_MESHES_WITH_SIMD
    int meshes_count = 0;

    for (RenderObject const& o : render_objects)
    {
        // LOLZ, no need for the "* check" because ALL objects will have a 
        // std::vector of triangles that will nicely return 0 when asked for its size
        // But lets do it for the sake of branchless 
        bool check = (o.type == RenderObjectType_Mesh);
        meshes_count += check;
        triangles_count += o.mesh_data.triangles.size() * check;
    }

#if NASTY_CPP_SIMD_THAT_HIDES_WHAT_HAPPENS
    if (world_triangles_packed == NULL)
    {
        int trail_tris = triangles_count%SIMD_FLOAT_WIDTH;

        wide_tris_count = (triangles_count/SIMD_FLOAT_WIDTH) + (trail_tris > 0);
        world_triangles_packed = aligned_alloc<wide_triangles>(32, wide_tris_count);

#if 0
        printf("total tris   : %d\n", triangles_count);
        printf("packable tris: %d\n", triangles_count-trail_tris);
        printf("trail tris   : %d\n", trail_tris);
        printf("wide tris    : %d\n", wide_tris_count);
#endif
    }
#else
    total_tri_buffer_size = triangles_count + (triangles_count % SIMD_FLOAT_WIDTH);
    assert(total_tri_buffer_size % SIMD_FLOAT_WIDTH == 0);

    tri_a_x = aligned_alloc<float>(32, total_tri_buffer_size);
    tri_a_y = aligned_alloc<float>(32, total_tri_buffer_size);
    tri_a_z = aligned_alloc<float>(32, total_tri_buffer_size);
    tri_b_x = aligned_alloc<float>(32, total_tri_buffer_size);
    tri_b_y = aligned_alloc<float>(32, total_tri_buffer_size);
    tri_b_z = aligned_alloc<float>(32, total_tri_buffer_size);
    tri_c_x = aligned_alloc<float>(32, total_tri_buffer_size);
    tri_c_y = aligned_alloc<float>(32, total_tri_buffer_size);
    tri_c_z = aligned_alloc<float>(32, total_tri_buffer_size);
    tri_material_id = aligned_alloc<float>(32, total_tri_buffer_size);
#endif
#endif
}

void preprocess_meshes_into_wide_data()
{
#if PROCESS_MESHES_WITH_SIMD
#if NASTY_CPP_SIMD_THAT_HIDES_WHAT_HAPPENS
    int pending_triangles = triangles_count;
    int current_inpack_tri_index = 0;
    int current_wtri_index = 0;
    for (RenderObject const& o : render_objects)
    {
        if (o.type == RenderObjectType_Mesh)
        {
            for (triangle const& t : o.mesh_data.triangles)
            {
                wide_triangles* wt = &world_triangles_packed[current_wtri_index];
                SET_WVEC3_FIELD(wt, t, current_inpack_tri_index, a);
                SET_WVEC3_FIELD(wt, t, current_inpack_tri_index, b);
                SET_WVEC3_FIELD(wt, t, current_inpack_tri_index, c);
                SET_WVEC3_FIELD(wt, t, current_inpack_tri_index, n);
                wt->material_ids[current_inpack_tri_index] = (float)o.material_id;

#if 0
                printf("source tri: %s %s %s | ", t.a.c_str(),
                        t.b.c_str(),
                        t.c.c_str());
                printf("material: %d | ", o.material_id);
                printf("\n");

                ///////"source tri: %s %s %s | ", ....
                printf("wide tri  : %s %s %s | ", wt->a.vec3_from_line(current_inpack_tri_index).c_str(),
                        wt->b.vec3_from_line(current_inpack_tri_index).c_str(),
                        wt->c.vec3_from_line(current_inpack_tri_index).c_str());
                printf("material: %d | ", (int)wt->material_ids[current_inpack_tri_index]);
                printf("wide index: %d, %d | ", current_wtri_index, current_inpack_tri_index);
                printf("pending triangles: %d | ", pending_triangles);
                printf("\n");
#endif
                --pending_triangles;
                ++current_inpack_tri_index;
                current_wtri_index += (current_inpack_tri_index == SIMD_FLOAT_WIDTH);
                current_inpack_tri_index %= SIMD_FLOAT_WIDTH;
            }
        }
    }

    while (current_inpack_tri_index < SIMD_FLOAT_WIDTH && current_wtri_index < wide_tris_count)
    {
        triangle t(vec3::zero(), vec3::zero(), vec3::zero());
        wide_triangles* wt = &world_triangles_packed[current_wtri_index];
        SET_WVEC3_FIELD(wt, t, current_inpack_tri_index, a);
        SET_WVEC3_FIELD(wt, t, current_inpack_tri_index, b);
        SET_WVEC3_FIELD(wt, t, current_inpack_tri_index, c);
        SET_WVEC3_FIELD(wt, t, current_inpack_tri_index, n);
        wt->material_ids[current_inpack_tri_index] = -1.0f;
#if 0
        printf("wide tri  : %s %s %s | ", wt->a.vec3_from_line(current_inpack_tri_index).c_str(),
                wt->b.vec3_from_line(current_inpack_tri_index).c_str(),
                wt->c.vec3_from_line(current_inpack_tri_index).c_str());
        printf("material: %d | ", (int)wt->material_ids[current_inpack_tri_index]);
        printf("wide index: %d, %d | ", current_wtri_index, current_inpack_tri_index);
        printf("pending triangles: %d | ", pending_triangles);
        printf("cleaned!\n");
#endif
        ++current_inpack_tri_index;
        current_wtri_index += (current_inpack_tri_index == SIMD_FLOAT_WIDTH);
        current_inpack_tri_index %= SIMD_FLOAT_WIDTH;
    }
#else
    int current_inpack_tri_index = 0;
    int current_triangle_index = 0;
    for (RenderObject const& o : render_objects)
    {
        if (o.type == RenderObjectType_Mesh)
        {
            for (triangle const& t : o.mesh_data.triangles)
            {
                tri_a_x[current_triangle_index] = t.a.x;
                tri_a_y[current_triangle_index] = t.a.y;
                tri_a_z[current_triangle_index] = t.a.z;
                tri_b_x[current_triangle_index] = t.b.x;
                tri_b_y[current_triangle_index] = t.b.y;
                tri_b_z[current_triangle_index] = t.b.z;
                tri_c_x[current_triangle_index] = t.c.x;
                tri_c_y[current_triangle_index] = t.c.y;
                tri_c_z[current_triangle_index] = t.c.z;
                tri_material_id[current_triangle_index] = (float)o.material_id;
                ++current_triangle_index;
            }
        }
    }

    while (current_triangle_index < total_tri_buffer_size)
    {
        tri_a_x[current_triangle_index] = 0.0f;
        tri_a_y[current_triangle_index] = 0.0f;
        tri_a_z[current_triangle_index] = 0.0f;
        tri_b_x[current_triangle_index] = 0.0f;
        tri_b_y[current_triangle_index] = 0.0f;
        tri_b_z[current_triangle_index] = 0.0f;
        tri_c_x[current_triangle_index] = 0.0f;
        tri_c_y[current_triangle_index] = 0.0f;
        tri_c_z[current_triangle_index] = 0.0f;
        tri_material_id[current_triangle_index] = -1.0f;
        ++current_triangle_index;
    }

    printf("\nDATA CONVERTION DONE!\n");
#endif
#endif
    //exit(0);
}

color skybox_color(vec3 direction)
{
    vec3 unit_direction = unit_vector(direction);
    float t = 0.5f * (unit_direction.y + 1.0f);
    return ((1.0f-t) * color(1.0f, 1.0f, 1.0f) + t * color(0.5f, 0.7f, 1.0f));
}

bool w_check_aabb_ray_axis(__m256 ray_orig, __m256 ray_dir, __m256 aabb_min, __m256 aabb_max, __m256 dist_min, __m256 dist_max)
{
    __m256 invD = _mm256_div_ps(_mm256_set1_ps(1.0f), ray_dir);
    __m256 t0 = _mm256_min_ps(_mm256_mul_ps(_mm256_sub_ps(aabb_min, ray_orig), invD),
                              _mm256_mul_ps(_mm256_sub_ps(aabb_max, ray_orig), invD));
    __m256 t1 = _mm256_max_ps(_mm256_mul_ps(_mm256_sub_ps(aabb_min, ray_orig), invD),
                              _mm256_mul_ps(_mm256_sub_ps(aabb_max, ray_orig), invD));
    __m256 check_t_min = _mm256_max_ps(t0, dist_min);
    __m256 check_t_max = _mm256_min_ps(t1, dist_max);
    __m256 aabb_check = _mm256_cmp_ps(check_t_max, check_t_min, _CMP_GT_OQ);

    int mask = _mm256_movemask_ps(aabb_check);

    return mask != 0;
}

#define ASSIGN_RAY(_x, _y, _z, source, value)\
    {for (int __i = 0; __i < 8; ++__i)\
    {\
        _x[__i] = source[__i].value.x;\
        _y[__i] = source[__i].value.y;\
        _z[__i] = source[__i].value.z;\
    }}

struct w_hits
{
    __m256 t;
    __m256 material;
    __m256 n_x;
    __m256 n_y;
    __m256 n_z;

    inline void mask_set(__m256 w_new_t, __m256 w_new_material_ids,
                         __m256 new_n_x, __m256 new_n_y, __m256 new_n_z,
                         __m256 w_mask)
    {
        t = _mm256_blendv_ps(t, w_new_t, w_mask);
        material = _mm256_blendv_ps(material, w_new_material_ids, w_mask);
        n_x = _mm256_blendv_ps(n_x, new_n_x, w_mask);
        n_y = _mm256_blendv_ps(n_y, new_n_y, w_mask);
        n_z = _mm256_blendv_ps(n_z, new_n_z, w_mask);
    }

    inline void mask_set(w_hits const& other, __m256 w_mask)
    {
        mask_set(other.t, other.material, other.n_x, other.n_y, other.n_z, w_mask);
    }
};

w_hits simd_ray_hits(RenderObjectID object_id, 
        __m256 w_ray_dir_x, __m256 w_ray_dir_y, __m256 w_ray_dir_z,
        __m256 w_ray_orig_x, __m256 w_ray_orig_y, __m256 w_ray_orig_z,
        __m256 w_t_min, __m256 w_t_max)
{
    w_hits results;

    RenderObject const& o = *RenderObject_Get(object_id);

    results.t = w_infinity;
    results.material = _mm256_set1_ps(-1.0f);

    bool check_object =
        w_check_aabb_ray_axis(w_ray_orig_x, w_ray_dir_x, _mm256_set1_ps(o.bounding_box.minimum.x), _mm256_set1_ps(o.bounding_box.maximum.x), w_t_min, w_t_max)
        && w_check_aabb_ray_axis(w_ray_orig_y, w_ray_dir_y, _mm256_set1_ps(o.bounding_box.minimum.y), _mm256_set1_ps(o.bounding_box.maximum.y), w_t_min, w_t_max)
        && w_check_aabb_ray_axis(w_ray_orig_z, w_ray_dir_z, _mm256_set1_ps(o.bounding_box.minimum.z), _mm256_set1_ps(o.bounding_box.maximum.z), w_t_min, w_t_max);

    if (!check_object)
    {
        return results;
    }

    __m256 material_ids = _mm256_set1_ps((float)o.material_id);

    switch (o.type) {
        case RenderObjectType_BVHNode:
            {
                w_hits left_hits = simd_ray_hits(o.node_data.left_id, 
                        w_ray_dir_x,  w_ray_dir_y,  w_ray_dir_z,
                        w_ray_orig_x,  w_ray_orig_y,  w_ray_orig_z,
                        w_t_min,  w_t_max);

                __m256 better_hits_mask = _mm256_cmp_ps(left_hits.t, results.t, _CMP_LT_OQ);
                results.mask_set(left_hits, better_hits_mask);

                if (o.node_data.left_id != o.node_data.right_id)
                {
                    __m256 w_new_t_max = _mm256_min_ps(left_hits.t, w_t_max);

                    w_hits right_hits = simd_ray_hits(o.node_data.right_id, 
                            w_ray_dir_x,  w_ray_dir_y,  w_ray_dir_z,
                            w_ray_orig_x,  w_ray_orig_y,  w_ray_orig_z,
                            w_t_min,  w_new_t_max);

                    better_hits_mask = _mm256_cmp_ps(right_hits.t, results.t, _CMP_LT_OQ);
                    results.mask_set(right_hits, better_hits_mask);
                }
            }
            break;
        case RenderObjectType_List:
            {
                for (RenderObjectID id : o.list_data.objects)
                {
                    w_hits current_hits = simd_ray_hits(id, 
                            w_ray_dir_x,  w_ray_dir_y,  w_ray_dir_z,
                            w_ray_orig_x,  w_ray_orig_y,  w_ray_orig_z,
                            w_t_min,  w_t_max);

                    __m256 better_hits_mask = _mm256_cmp_ps(current_hits.t, results.t, _CMP_LT_OQ);
                    results.mask_set(current_hits, better_hits_mask);
                }
            }
            break;
        case RenderObjectType_Sphere:
            {
                __m256 w_sphere_center_x = _mm256_set1_ps(o.sphere_data.center.x);
                __m256 w_sphere_center_y = _mm256_set1_ps(o.sphere_data.center.y);
                __m256 w_sphere_center_z = _mm256_set1_ps(o.sphere_data.center.z);
                __m256 w_sphere_r = _mm256_set1_ps(o.sphere_data.radius);

                __m256 w_oc_x = _mm256_sub_ps(w_ray_orig_x, w_sphere_center_x);
                __m256 w_oc_y = _mm256_sub_ps(w_ray_orig_y, w_sphere_center_y);
                __m256 w_oc_z = _mm256_sub_ps(w_ray_orig_z, w_sphere_center_z);

                __m256 w_a = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(w_ray_dir_x, w_ray_dir_x),
                            _mm256_mul_ps(w_ray_dir_y, w_ray_dir_y)),
                            _mm256_mul_ps(w_ray_dir_z, w_ray_dir_z));

                __m256 w_half_b = m256_dot(w_oc_x     , w_oc_y     , w_oc_z,
                                           w_ray_dir_x, w_ray_dir_y, w_ray_dir_z);

                __m256 w_c = _mm256_sub_ps(m256_sq_len(w_oc_x, w_oc_y, w_oc_z),
                                          _mm256_mul_ps(w_sphere_r, w_sphere_r));

                __m256 w_discriminant = _mm256_sub_ps(_mm256_mul_ps(w_half_b, w_half_b),
                                                      _mm256_mul_ps(w_a, w_c));

                __m256 w_sqrtd = _mm256_sqrt_ps(w_discriminant);

                __m256 w_neg_half_b = _mm256_sub_ps(w_zero, w_half_b);
                __m256 w_root1 = _mm256_div_ps(_mm256_sub_ps(w_neg_half_b, w_sqrtd), w_a);
                __m256 w_root2 = _mm256_div_ps(_mm256_add_ps(w_neg_half_b, w_sqrtd), w_a);

                __m256 w_root1_mask = _mm256_and_ps(_mm256_cmp_ps(w_t_min, w_root1, _CMP_LT_OQ), _mm256_cmp_ps(w_root1, w_t_max, _CMP_LT_OQ));
                __m256 w_root2_mask = _mm256_andnot_ps(w_root1_mask,
                                        _mm256_and_ps(_mm256_cmp_ps(w_t_min, w_root2, _CMP_LT_OQ),
                                                      _mm256_cmp_ps(w_root2, w_t_max, _CMP_LT_OQ)));

                __m256 w_any_root_mask = _mm256_or_ps(w_root1_mask, w_root2_mask);

                __m256 current_t;
                current_t = _mm256_blendv_ps(w_infinity, w_root1, w_root1_mask);;
                current_t = _mm256_blendv_ps(current_t, w_root2, w_root2_mask);

                __m256 p_x = _mm256_add_ps(w_ray_orig_x, _mm256_mul_ps(w_ray_dir_x, current_t));
                __m256 p_y = _mm256_add_ps(w_ray_orig_y, _mm256_mul_ps(w_ray_dir_y, current_t));
                __m256 p_z = _mm256_add_ps(w_ray_orig_z, _mm256_mul_ps(w_ray_dir_z, current_t));
                __m256 n_x = _mm256_div_ps(_mm256_sub_ps(p_x, w_sphere_center_x), w_sphere_r);
                __m256 n_y = _mm256_div_ps(_mm256_sub_ps(p_y, w_sphere_center_y), w_sphere_r);
                __m256 n_z = _mm256_div_ps(_mm256_sub_ps(p_z, w_sphere_center_z), w_sphere_r);
                m256_normalize(&n_x, &n_y, &n_z);
                //vec3 outward_normal = (result->p - object->sphere_data.center) / object->sphere_data.radius;

                __m256 better_hits_mask = _mm256_cmp_ps(w_discriminant, w_zero, _CMP_GE_OQ);
                better_hits_mask = _mm256_and_ps(better_hits_mask, w_any_root_mask);
                better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, results.t, _CMP_LT_OQ));

                results.mask_set(current_t, material_ids, n_x, n_y, n_z, better_hits_mask);
            }
            break;
        case RenderObjectType_Mesh:
            for (auto const& triangle : o.mesh_data.triangles)
            {
                __m256 a_x = _mm256_set1_ps(triangle.a.x);
                __m256 a_y = _mm256_set1_ps(triangle.a.y);
                __m256 a_z = _mm256_set1_ps(triangle.a.z);
                __m256 b_x = _mm256_set1_ps(triangle.b.x);
                __m256 b_y = _mm256_set1_ps(triangle.b.y);
                __m256 b_z = _mm256_set1_ps(triangle.b.z);
                __m256 c_x = _mm256_set1_ps(triangle.c.x);
                __m256 c_y = _mm256_set1_ps(triangle.c.y);
                __m256 c_z = _mm256_set1_ps(triangle.c.z);

                __m256 v0v1_x = _mm256_sub_ps(b_x, a_x);
                __m256 v0v1_y = _mm256_sub_ps(b_y, a_y);
                __m256 v0v1_z = _mm256_sub_ps(b_z, a_z);

                __m256 v0v2_x = _mm256_sub_ps(c_x, a_x);
                __m256 v0v2_y = _mm256_sub_ps(c_y, a_y);
                __m256 v0v2_z = _mm256_sub_ps(c_z, a_z);

                __m256 n_x, n_y, n_z;
                m256_cross(v0v1_x, v0v1_y, v0v1_z,
                           v0v2_x, v0v2_y, v0v2_z,
                           &n_x, &n_y, &n_z);
                m256_normalize(&n_x, &n_y, &n_z);

                __m256 pvec_x = _mm256_sub_ps(_mm256_mul_ps(w_ray_dir_y, v0v2_z),
                        _mm256_mul_ps(w_ray_dir_z, v0v2_y));
                __m256 pvec_y = _mm256_sub_ps(_mm256_mul_ps(w_ray_dir_z, v0v2_x),
                        _mm256_mul_ps(w_ray_dir_x, v0v2_z));
                __m256 pvec_z = _mm256_sub_ps(_mm256_mul_ps(w_ray_dir_x, v0v2_y),
                        _mm256_mul_ps(w_ray_dir_y, v0v2_x));

                __m256 det = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(v0v1_x, pvec_x),
                            _mm256_mul_ps(v0v1_y, pvec_y)),
                        _mm256_mul_ps(v0v1_z, pvec_z));

                __m256 invDet = _mm256_div_ps(w_one, det);

                __m256 tvec_x = _mm256_sub_ps(w_ray_orig_x, a_x);
                __m256 tvec_y = _mm256_sub_ps(w_ray_orig_y, a_y);
                __m256 tvec_z = _mm256_sub_ps(w_ray_orig_z, a_z);

                __m256 t_p_dot = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(tvec_x, pvec_x),
                            _mm256_mul_ps(tvec_y, pvec_y)),
                        _mm256_mul_ps(tvec_z, pvec_z));

                __m256 u = _mm256_mul_ps(t_p_dot, invDet);

                __m256 qvec_x = _mm256_sub_ps(_mm256_mul_ps(tvec_y, v0v1_z),
                        _mm256_mul_ps(tvec_z, v0v1_y));
                __m256 qvec_y = _mm256_sub_ps(_mm256_mul_ps(tvec_z, v0v1_x),
                        _mm256_mul_ps(tvec_x, v0v1_z));
                __m256 qvec_z = _mm256_sub_ps(_mm256_mul_ps(tvec_x, v0v1_y),
                        _mm256_mul_ps(tvec_y, v0v1_x));

                __m256 dir_q_dot = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(w_ray_dir_x, qvec_x),
                            _mm256_mul_ps(w_ray_dir_y, qvec_y)),
                        _mm256_mul_ps(w_ray_dir_z, qvec_z));
                __m256 v = _mm256_mul_ps(dir_q_dot, invDet);

                __m256 v0v2_q_dot = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(v0v2_x, qvec_x),
                            _mm256_mul_ps(v0v2_y, qvec_y)),
                        _mm256_mul_ps(v0v2_z, qvec_z));
                __m256 current_t = _mm256_mul_ps(v0v2_q_dot, invDet);
                __m256 u_plus_v = _mm256_add_ps(u, v);
                __m256 better_hits_mask = _mm256_cmp_ps(det, w_epsilon, _CMP_GE_OQ);
                better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u, w_zero, _CMP_GE_OQ));
                better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u, w_one, _CMP_LE_OQ));
                better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(v, w_zero, _CMP_GE_OQ));
                better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u_plus_v, w_one, _CMP_LE_OQ));
                better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_t_min, _CMP_GE_OQ));
                better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_t_max, _CMP_LE_OQ));
                better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, results.t, _CMP_LT_OQ));

                results.mask_set(current_t, material_ids, n_x, n_y, n_z, better_hits_mask);
            }

            break;
    }
    return results;
}

void simd_ray_color(ray* in_rays, color* out_colors, RenderObjectID world, int depth, float min_distance = 0.001f, float max_distance = infinity)
{
    ray current_rays[SIMD_FLOAT_WIDTH];
    memcpy(current_rays, in_rays, SIMD_FLOAT_WIDTH*sizeof(ray));

    int must_process_ray[SIMD_FLOAT_WIDTH];

    for (int i = 0; i < SIMD_FLOAT_WIDTH; ++i)
    {
        out_colors[i] = color(0.8f, 0.8f, 0.9f);
        must_process_ray[i] = 1;
    }

    for (int i = 0; i < depth; ++i)
    {
        __m256 w_ray_dir_x, w_ray_dir_y, w_ray_dir_z;
        ASSIGN_RAY(w_ray_dir_x, w_ray_dir_y, w_ray_dir_z, current_rays, direction);

        __m256 w_ray_orig_x, w_ray_orig_y, w_ray_orig_z;
        ASSIGN_RAY(w_ray_orig_x ,w_ray_orig_y ,w_ray_orig_z, current_rays, origin);

        __m256 w_t_min = _mm256_set1_ps(min_distance);
        __m256 w_t_max = _mm256_set1_ps(max_distance);

        w_hits hits = simd_ray_hits(world, 
                w_ray_dir_x, w_ray_dir_y, w_ray_dir_z,
                w_ray_orig_x, w_ray_orig_y, w_ray_orig_z,
                w_t_min, w_t_max);

        // Did we get any material? If so, then we hit at least one triangle and
        // we need to calculate the colors and bounces
        __m256 cmp = _mm256_cmp_ps(hits.material, w_zero, _CMP_GE_OQ);
        int batch_mask = _mm256_movemask_ps(cmp);

        if (batch_mask != 0)
        {
            int ray_mask = 1;
            int stop_checking = 1;

            for (int check_index = 0; check_index < SIMD_FLOAT_WIDTH; ++check_index)
            {
                ray const& current_ray = current_rays[check_index];

                if (must_process_ray[check_index])
                {
                    if (batch_mask & ray_mask)
                    {
#if 0
                        printf("hit info %d :\n\tt: %0.2f\n\tm: %d\n", check_index, hits.t[check_index], (int)hits.material[check_index]);
#endif
                        hit_record hit;
                        hit.t = hits.t[check_index];
                        hit.p = current_ray.at(hit.t);
                        hit.material_id = (int)hits.material[check_index];
                        hit.set_face_normal(current_ray,
                                            vec3(hits.n_x[check_index],
                                                 hits.n_y[check_index],
                                                 hits.n_z[check_index]));

                        ray scattered;
                        color attenuation;
                        if (Material_Get(hit.material_id)->scatter(current_ray, hit, &attenuation, &scattered))
                        {
                            current_rays[check_index] = scattered;
                            out_colors[check_index] *= attenuation;
                            stop_checking = 0;
                        }
                        else
                        {
                            //printf(" > WARNING: Material didn't scattered. What does this mean?\n");
                            out_colors[check_index] *= color::zero();
                            must_process_ray[check_index] = 0;
                        }
                    }
                    else
                    {
                        out_colors[check_index] *= skybox_color(current_ray.direction);
                        must_process_ray[check_index] = 0;
                    }
                }

                ray_mask <<= 1;
            }

            if (stop_checking)
            {
                return;
            }
        }
        else
        {
            for (int check_index = 0; check_index < SIMD_FLOAT_WIDTH; ++check_index)
            {
                if (must_process_ray[check_index])
                {
                    out_colors[check_index] *= skybox_color(current_rays[check_index].direction);
                    must_process_ray[check_index] = 0;
                }
            }

            return;
        }
    }
}

color ray_color(ray const& original_ray, RenderObjectID world, int depth)
{
    float min_distance = 0.001f;
    ray current_ray(original_ray);
    hit_record hit;
    color result_color(0.8f, 0.8f, 0.9f);
#if PROCESS_MESHES_WITH_SIMD
    __m256 w_t_max = w_infinity;
    __m256 w_t_min = _mm256_set1_ps(min_distance);
#endif

    for (int i = 0; i < depth; ++i)
    {
#if PROCESS_MESHES_WITH_SIMD
#if NASTY_CPP_SIMD_THAT_HIDES_WHAT_HAPPENS
        w_vec3 w_ray_dir = w_vec3(current_ray.direction);
        w_vec3 w_ray_orig = w_vec3(current_ray.origin);
        __m256 w_best_t = w_infinity;
        __m256 w_best_material = _mm256_set1_ps(-1.0f);
        w_vec3 w_best_v0v1;
        w_vec3 w_best_v0v2;

        for (int wtri_index = 0; wtri_index < wide_tris_count; ++wtri_index)
        {
            wide_triangles const& wt = world_triangles_packed[wtri_index];
            w_vec3 v0v1 = wt.b - wt.a;
            w_vec3 v0v2 = wt.c - wt.a;
            w_vec3 pvec = cross(w_ray_dir, v0v2);
            __m256 det = dot(v0v1, pvec);
            __m256 invDet = _mm256_div_ps(w_one, det);
            w_vec3 tvec = w_ray_orig - wt.a;
            __m256 u = _mm256_mul_ps(dot(tvec, pvec), invDet);
            w_vec3 qvec = cross(tvec, v0v1);
            __m256 v = _mm256_mul_ps(dot(w_ray_dir, qvec), invDet);
            __m256 current_t = _mm256_mul_ps(dot(v0v2, qvec), invDet);

            __m256 u_plus_v = _mm256_add_ps(u, v);
            __m256 better_hits_mask = _mm256_cmp_ps(det, w_epsilon, _CMP_GE_OQ);
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u, w_zero, _CMP_GE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u, w_one, _CMP_LE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(v, w_zero, _CMP_GE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u_plus_v, w_one, _CMP_LE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_t_min, _CMP_GE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_t_max, _CMP_LE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_best_t, _CMP_LT_OQ));

            w_best_material = _mm256_blendv_ps(w_best_material, wt.material_ids, better_hits_mask);
            w_best_t        = _mm256_blendv_ps(w_best_t       , current_t      , better_hits_mask);
            w_best_v0v1.x   = _mm256_blendv_ps(w_best_v0v1.x  , v0v1.x         , better_hits_mask);
            w_best_v0v1.y   = _mm256_blendv_ps(w_best_v0v1.y  , v0v1.y         , better_hits_mask);
            w_best_v0v1.z   = _mm256_blendv_ps(w_best_v0v1.z  , v0v1.z         , better_hits_mask);
            w_best_v0v2.x   = _mm256_blendv_ps(w_best_v0v2.x  , v0v2.x         , better_hits_mask);
            w_best_v0v2.y   = _mm256_blendv_ps(w_best_v0v2.y  , v0v2.y         , better_hits_mask);
            w_best_v0v2.z   = _mm256_blendv_ps(w_best_v0v2.z  , v0v2.z         , better_hits_mask);
        }
#else
        __m256 w_ray_dir_x = _mm256_set1_ps(current_ray.direction.x);
        __m256 w_ray_dir_y = _mm256_set1_ps(current_ray.direction.y);
        __m256 w_ray_dir_z = _mm256_set1_ps(current_ray.direction.z);

        __m256 w_ray_orig_x = _mm256_set1_ps(current_ray.origin.x);
        __m256 w_ray_orig_y = _mm256_set1_ps(current_ray.origin.y);
        __m256 w_ray_orig_z = _mm256_set1_ps(current_ray.origin.z);

        __m256 w_best_t = w_infinity;
        __m256 w_best_material = _mm256_set1_ps(-1.0f);
        __m256 w_best_v0v1_x;
        __m256 w_best_v0v1_y;
        __m256 w_best_v0v1_z;
        __m256 w_best_v0v2_x;
        __m256 w_best_v0v2_y;
        __m256 w_best_v0v2_z;

        for (int tri_index = 0; tri_index < triangles_count; tri_index += SIMD_FLOAT_WIDTH)
        {
            __m256 a_x = _mm256_load_ps(&tri_a_x[tri_index]);
            __m256 a_y = _mm256_load_ps(&tri_a_y[tri_index]);
            __m256 a_z = _mm256_load_ps(&tri_a_z[tri_index]);
            __m256 b_x = _mm256_load_ps(&tri_b_x[tri_index]);
            __m256 b_y = _mm256_load_ps(&tri_b_y[tri_index]);
            __m256 b_z = _mm256_load_ps(&tri_b_z[tri_index]);
            __m256 c_x = _mm256_load_ps(&tri_c_x[tri_index]);
            __m256 c_y = _mm256_load_ps(&tri_c_y[tri_index]);
            __m256 c_z = _mm256_load_ps(&tri_c_z[tri_index]);
            __m256 material_ids = _mm256_load_ps(&tri_material_id[tri_index]);

            __m256 v0v1_x = _mm256_sub_ps(b_x, a_x);
            __m256 v0v1_y = _mm256_sub_ps(b_y, a_y);
            __m256 v0v1_z = _mm256_sub_ps(b_z, a_z);

            __m256 v0v2_x = _mm256_sub_ps(c_x, a_x);
            __m256 v0v2_y = _mm256_sub_ps(c_y, a_y);
            __m256 v0v2_z = _mm256_sub_ps(c_z, a_z);

            __m256 pvec_x = _mm256_sub_ps(_mm256_mul_ps(w_ray_dir_y, v0v2_z),
                                          _mm256_mul_ps(w_ray_dir_z, v0v2_y));
            __m256 pvec_y = _mm256_sub_ps(_mm256_mul_ps(w_ray_dir_z, v0v2_x),
                                          _mm256_mul_ps(w_ray_dir_x, v0v2_z));
            __m256 pvec_z = _mm256_sub_ps(_mm256_mul_ps(w_ray_dir_x, v0v2_y),
                                          _mm256_mul_ps(w_ray_dir_y, v0v2_x));

            __m256 det = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(v0v1_x, pvec_x),
                            _mm256_mul_ps(v0v1_y, pvec_y)),
                            _mm256_mul_ps(v0v1_z, pvec_z));

            __m256 invDet = _mm256_div_ps(w_one, det);

            __m256 tvec_x = _mm256_sub_ps(w_ray_orig_x, a_x);
            __m256 tvec_y = _mm256_sub_ps(w_ray_orig_y, a_y);
            __m256 tvec_z = _mm256_sub_ps(w_ray_orig_z, a_z);

            __m256 t_p_dot = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(tvec_x, pvec_x),
                            _mm256_mul_ps(tvec_y, pvec_y)),
                            _mm256_mul_ps(tvec_z, pvec_z));

            __m256 u = _mm256_mul_ps(t_p_dot, invDet);

            __m256 qvec_x = _mm256_sub_ps(_mm256_mul_ps(tvec_y, v0v1_z),
                                          _mm256_mul_ps(tvec_z, v0v1_y));
            __m256 qvec_y = _mm256_sub_ps(_mm256_mul_ps(tvec_z, v0v1_x),
                                          _mm256_mul_ps(tvec_x, v0v1_z));
            __m256 qvec_z = _mm256_sub_ps(_mm256_mul_ps(tvec_x, v0v1_y),
                                          _mm256_mul_ps(tvec_y, v0v1_x));


            __m256 dir_q_dot = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(w_ray_dir_x, qvec_x),
                            _mm256_mul_ps(w_ray_dir_y, qvec_y)),
                            _mm256_mul_ps(w_ray_dir_z, qvec_z));
            __m256 v = _mm256_mul_ps(dir_q_dot, invDet);

            __m256 v0v2_q_dot = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(v0v2_x, qvec_x),
                            _mm256_mul_ps(v0v2_y, qvec_y)),
                            _mm256_mul_ps(v0v2_z, qvec_z));
            __m256 current_t = _mm256_mul_ps(v0v2_q_dot, invDet);
            __m256 u_plus_v = _mm256_add_ps(u, v);
            __m256 better_hits_mask = _mm256_cmp_ps(det, w_epsilon, _CMP_GE_OQ);
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u, w_zero, _CMP_GE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u, w_one, _CMP_LE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(v, w_zero, _CMP_GE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u_plus_v, w_one, _CMP_LE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_t_min, _CMP_GE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_t_max, _CMP_LE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_best_t, _CMP_LT_OQ));

            w_best_material = _mm256_blendv_ps(w_best_material, material_ids, better_hits_mask);
            w_best_t        = _mm256_blendv_ps(w_best_t       , current_t   , better_hits_mask);
            w_best_v0v1_x   = _mm256_blendv_ps(w_best_v0v1_x  , v0v1_x      , better_hits_mask);
            w_best_v0v1_y   = _mm256_blendv_ps(w_best_v0v1_y  , v0v1_y      , better_hits_mask);
            w_best_v0v1_z   = _mm256_blendv_ps(w_best_v0v1_z  , v0v1_z      , better_hits_mask);
            w_best_v0v2_x   = _mm256_blendv_ps(w_best_v0v2_x  , v0v2_x      , better_hits_mask);
            w_best_v0v2_y   = _mm256_blendv_ps(w_best_v0v2_y  , v0v2_y      , better_hits_mask);
            w_best_v0v2_z   = _mm256_blendv_ps(w_best_v0v2_z  , v0v2_z      , better_hits_mask);
        }
#endif

        // Did we get any material? If so, then we hit at least one triangle and
        // we need to find the best hit and process it
        __m256 cmp = _mm256_cmp_ps(w_best_material, w_zero, _CMP_GE_OQ);
        int mask = _mm256_movemask_ps(cmp);
        bool did_hit = mask != 0;

        if (did_hit)
        {
            int best_index = -1;

            for (int hit_index = 0; hit_index < SIMD_FLOAT_WIDTH; ++hit_index)
            {
                if (w_best_material[hit_index] >= 0.0f && w_best_t[hit_index] > min_distance && (best_index < 0 || w_best_t[hit_index] < w_best_t[best_index]))
                {
                    best_index = hit_index;
                }

#if 0
                printf("hit info %d :\n\tt: %0.2f\n\tm: %d\n", hit_index, 
                        w_best_t[hit_index], (int)w_best_material[hit_index]);
#endif
            }

#if NASTY_CPP_SIMD_THAT_HIDES_WHAT_HAPPENS
            vec3 v0v1 = w_best_v0v1.vec3_from_line(best_index);
            vec3 v0v2 = w_best_v0v2.vec3_from_line(best_index);
#else
            vec3 v0v1(w_best_v0v1_x[best_index],w_best_v0v1_y[best_index],  w_best_v0v1_z[best_index]);
            vec3 v0v2(w_best_v0v2_x[best_index],w_best_v0v2_y[best_index],  w_best_v0v2_z[best_index]);
#endif
            hit.t = w_best_t[best_index];
            hit.normal = cross(v0v1, v0v2).normal();
            hit.p = current_ray.at(hit.t);
            hit.front_face = true;
            hit.material_id = (int)w_best_material[best_index];

#if 0
            printf("BEST HIT info %d:\n\tt: %0.2f\n\tm: %d\n\tn: %s\tl: %0.2f\n", best_index, 
                    w_best_t[best_index], (int)w_best_material[best_index], hit.normal.c_str(), hit.normal.length());
#endif
        }

        //exit(10);

#else
        bool did_hit = RenderObject_HitRay(world, current_ray, 0.001f, infinity, &hit);
#endif

        if (did_hit)
        {
            ray scattered;
            color attenuation;
            if (Material_Get(hit.material_id)->scatter(current_ray, hit, &attenuation, &scattered))
            {
                current_ray = scattered;
                result_color *= attenuation;
            }
            else
            {
                //printf(" > WARNING: Material didn't scattered. What does this mean?\n");
                result_color *= color::zero();
                break;
            }
        }
        else
        {
            result_color *= skybox_color(current_ray.direction);
            break;
        }
    }

    return result_color;
}

#endif // !MUH_RENDER_CPP
