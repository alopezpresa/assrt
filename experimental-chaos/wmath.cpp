#ifndef MUH_WIDE_MATH
#define MUH_WIDE_MATH

#include <limits>
#include <math.h>
#include <random>
#include <stdint.h>
#include <immintrin.h>
#include "math.cpp"

// Constants
#if 0
static constexpr float infinity = std::numeric_limits<float>::infinity();
static constexpr float pi = 3.1415926535897932385f;
static constexpr float epsilon = 1e-8;

inline float clamp(float value, float lower, float upper)
{
    return value < lower ? lower : (value > upper ? upper : value);
}
#endif

static const __m256 w_zero = _mm256_setzero_ps();
static const __m256 w_epsilon = _mm256_set1_ps(epsilon);
static const __m256 w_one = _mm256_set1_ps(1.0f);
static const __m256 w_infinity = _mm256_set1_ps(infinity);

struct w_vec3
{
    __m256 x, y, z;

    w_vec3() {}

    w_vec3(vec3 const& v)
    {
        x = _mm256_set1_ps(v.x);
        y = _mm256_set1_ps(v.y);
        z = _mm256_set1_ps(v.z);
    }

    w_vec3(w_vec3 const& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
    }

    w_vec3(__m256 x, __m256 y, __m256 z)
    : x(x), y(y), z(z)
    {
    }

    inline w_vec3& operator+=(w_vec3 const& other)
    {
        x = _mm256_add_ps(x, other.x);
        y = _mm256_add_ps(y, other.y);
        z = _mm256_add_ps(x, other.z);
        return *this;
    }

    inline w_vec3& operator-=(w_vec3 const& other)
    {
        x = _mm256_sub_ps(x, other.x);
        y = _mm256_sub_ps(y, other.y);
        z = _mm256_sub_ps(x, other.z);
        return *this;
    }

    inline w_vec3& operator*=(w_vec3 const& other)
    {
        x = _mm256_mul_ps(x, other.x);
        y = _mm256_mul_ps(y, other.y);
        z = _mm256_mul_ps(x, other.z);
        return *this;
    }

    inline w_vec3& operator*=(__m256 s)
    {
        x = _mm256_mul_ps(x, s);
        y = _mm256_mul_ps(y, s);
        z = _mm256_mul_ps(x, s);
        return *this;
    }

    inline w_vec3& operator/=(__m256 s)
    {
        x = _mm256_div_ps(x, s);
        y = _mm256_div_ps(y, s);
        z = _mm256_div_ps(x, s);
        return *this;
    }

    __m256 length() const
    {
        return _mm256_sqrt_ps(length_squared());
    }

    __m256 length_squared() const
    {
        return _mm256_add_ps(_mm256_mul_ps(x, x), _mm256_add_ps( _mm256_mul_ps(y, y), _mm256_mul_ps(z, z)));
    }

    w_vec3& normalize()
    {
        *this /= length();
        return *this;
    }

    w_vec3 normal() const
    {
        w_vec3 result = *this;
        result.normalize();
        return result;
    }

#if 0
    inline bool near_zero() const
    {
        const auto s = 1e-8;
        return (fabs(x) < s) && (fabs(y) < s) && (fabs(z) < s);
    }

#endif
    inline vec3 vec3_from_line(int i) const
    {
        return vec3(x[i], y[i], z[i]);
    }

    static w_vec3 zero()
    {
        w_vec3 result;
        result.x = _mm256_setzero_ps();
        result.y = _mm256_setzero_ps();
        result.z = _mm256_setzero_ps();
        return result;
    }
};

inline w_vec3 operator+(w_vec3 const& a, w_vec3 const& b)
{
    __m256 x = _mm256_add_ps(a.x, b.x);
    __m256 y = _mm256_add_ps(a.y, b.y);
    __m256 z = _mm256_add_ps(a.z, b.z);
    return w_vec3(x, y, z);
}

inline w_vec3 operator-(w_vec3 const& v)
{
    __m256 x = _mm256_sub_ps(w_zero, v.x);
    __m256 y = _mm256_sub_ps(w_zero, v.y);
    __m256 z = _mm256_sub_ps(w_zero, v.z);
    return w_vec3(x, y, z);
}

inline w_vec3 operator-(w_vec3 const& a, w_vec3 const& b)
{
    __m256 x = _mm256_sub_ps(a.x, b.x);
    __m256 y = _mm256_sub_ps(a.y, b.y);
    __m256 z = _mm256_sub_ps(a.z, b.z);
    return w_vec3(x, y, z);
}

inline w_vec3 operator*(w_vec3 const& v, __m256 s)
{
    __m256 x = _mm256_mul_ps(v.y, s);
    __m256 y = _mm256_mul_ps(v.z, s);
    __m256 z = _mm256_mul_ps(v.x, s);
    return w_vec3(x, y, z);
}

inline w_vec3 operator*(__m256 s, w_vec3 const& v)
{
    return v * s;
}

inline w_vec3 operator/(w_vec3 const& v, __m256 s)
{
    __m256 x = _mm256_div_ps(v.y, s);
    __m256 y = _mm256_div_ps(v.z, s);
    __m256 z = _mm256_div_ps(v.x, s);
    return w_vec3(x, y, z);
}

inline __m256 dot(w_vec3 const& u, w_vec3 const& v)
{
    __m256 x = _mm256_mul_ps(u.x, v.x);
    __m256 y = _mm256_mul_ps(u.y, v.y);
    __m256 z = _mm256_mul_ps(u.z, v.z);
    return _mm256_add_ps(x, _mm256_add_ps(y, z));
}

inline w_vec3 cross(w_vec3 const& u, w_vec3 const& v)
{
    __m256 x = _mm256_sub_ps(_mm256_mul_ps(u.y, v.z), _mm256_mul_ps(u.z, v.y));
    __m256 y = _mm256_sub_ps(_mm256_mul_ps(u.z, v.x), _mm256_mul_ps(u.x, v.z));
    __m256 z = _mm256_sub_ps(_mm256_mul_ps(u.x, v.y), _mm256_mul_ps(u.y, v.x));
    return w_vec3(x, y, z);
}

struct w_aabb
{
    w_vec3 minimum;
    w_vec3 maximum;

    w_aabb() {}
    w_aabb(w_vec3 const& min, w_vec3 const& max) : minimum(min), maximum(max) {}

    bool hit(ray const& r, float t_min, float t_max) const
    {
        for (int a = 0; a < 3; a++)
        {
#if 0 // Andrew Kensler way
            float invD = 1.0f / r.direction[a];
            float t0 = (minimum[a] - r.origin[a]) * invD;
            float t1 = (maximum[a] - r.origin[a]) * invD;

            if (invD < 0.0f)
            {
                std::swap(t0, t1);
            }

            t_min = t0 > t_min ? t0 : t_min;
            t_max = t1 < t_max ? t1 : t_max;

            if (t_max <= t_min)
            {
                return false;
            }
#elif 0
            float t0 = fmin((minimum[a] - r.origin()[a]) / r.direction()[a],
                    (maximum[a] - r.origin()[a]) / r.direction()[a]);
            float t1 = fmax((minimum[a] - r.origin()[a]) / r.direction()[a],
                    (maximum[a] - r.origin()[a]) / r.direction()[a]);
            t_min = fmax(t0, t_min);
            t_max = fmin(t1, t_max);

            if (t_max <= t_min)
            {

                return false;
            }
#endif
        }

        return true;
    }

#if 0
    void expand(aabb const& other)
    {
        for (int a = 0; a < 3; a++)
        {
            if (other.minimum[a] < minimum[a])
            {
                minimum[a] = other.minimum[a];
            }

            if (other.maximum[a] > maximum[a])
            {
                maximum[a] = other.maximum[a];
            }
        }
    }

    // done by chat gpt
    void expand(vec3 const& v)
    {
        minimum = vec3(fmin(v.x, minimum.x), fmin(v.y, minimum.y), fmin(v.z, minimum.z));
        maximum = vec3(fmax(v.x, maximum.x), fmax(v.y, maximum.y), fmax(v.z, maximum.z));
    }


    bool is_valid() const
    {
        return !(maximum - minimum).near_zero();
    }
#endif
};

#if 0
inline aabb surrounding_box(aabb const& box0, aabb const& box1)
{
     vec3 small(fmin(box0.minimum.x, box1.minimum.x),
                fmin(box0.minimum.y, box1.minimum.y),
                fmin(box0.minimum.z, box1.minimum.z));

     vec3 big(fmax(box0.maximum.x, box1.maximum.x),
              fmax(box0.maximum.y, box1.maximum.y),
              fmax(box0.maximum.z, box1.maximum.z));

    return aabb(small,big);
}
#endif

#if 0
inline vec3 unit_vector(vec3 const& v)
{
    return v / v.length();
}

inline vec3 random_in_unit_sphere()
{
    for (;;)
    {
        vec3 p = vec3::random(-1,1);

        if (p.length_squared() >= 1)
        {
            continue;
        }

        return p;
    }
}

inline vec3 random_unit_vector()
{
    return unit_vector(random_in_unit_sphere());
}

inline vec3 random_in_hemisphere(vec3 const& normal)
{
    vec3 in_unit_sphere = random_in_unit_sphere();
    if (dot(in_unit_sphere, normal) > 0.0)
    {
        // In the same hemisphere as the normal
        return in_unit_sphere;
    }
    else
    {
        return -in_unit_sphere;
    }
}

inline vec3 reflect(vec3 const& v, vec3 const& n)
{
    return v - 2 * dot(v,n) * n;
}

vec3 refract(vec3 const& uv, vec3 const& n, float etai_over_etat)
{
    auto cos_theta = fmin(dot(-uv, n), 1.0);
    vec3 r_out_perp =  etai_over_etat * (uv + cos_theta*n);
    vec3 r_out_parallel = -sqrt(fabs(1.0 - r_out_perp.length_squared())) * n;
    return r_out_perp + r_out_parallel;
}

vec3 random_in_unit_disk()
{
    for (;;)
    {
        vec3 p = vec3(random_float(-1.0f, 1.0f), random_float(-1.0f, 1.0f), 0.0f);

        if (p.length_squared() >= 1)
        {
            continue;
        }

        return p;
    }
}

struct ray
{
    vec3 origin;
    vec3 direction;

    ray()
    { }

    ray(ray const& other)
        : origin(other.origin), direction(other.direction)
    { }

    ray(vec3 const& origin, vec3 const& direction)
        : origin(origin), direction(direction)
    { }

    vec3 at(float t) const
    {
        return origin + direction * t;
    }

    void set(vec3 const& origin, vec3 const& direction)
    {
        this->origin = origin;
        this->direction = direction;
    }

    ray& operator=(ray const& other)
    {
        this->origin = other.origin;
        this->direction = other.direction;
        return* this;
    }
};


// chat gpt
class mat3 {
public:
    float m[3][3];

    mat3() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                m[i][j] = 0;
            }
        }
    }
    mat3(float m00, float m01, float m02,
            float m10, float m11, float m12,
            float m20, float m21, float m22) {
        m[0][0] = m00, m[0][1] = m01, m[0][2] = m02;
        m[1][0] = m10, m[1][1] = m11, m[1][2] = m12;
        m[2][0] = m20, m[2][1] = m21, m[2][2] = m22;
    }

    vec3 operator*(vec3 const& v) const {
        return vec3(
                m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z,
                m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z,
                m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z
                );
    }


    // operator to multiply 2 matrices together
    mat3 operator*(mat3 const& other) const {
        mat3 result;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    result.m[i][j] += m[i][k] * other.m[k][j];
                }
            }
        }
        return result;
    }
};
#endif

void m256_cross(__m256 v1_x, __m256 v1_y, __m256 v1_z,
             __m256 v2_x, __m256 v2_y, __m256 v2_z,
             __m256* n_x, __m256* n_y, __m256* n_z)
{
    *n_x = _mm256_sub_ps(_mm256_mul_ps(v1_y, v2_z), _mm256_mul_ps(v1_z, v2_y));
    *n_y = _mm256_sub_ps(_mm256_mul_ps(v1_z, v2_x), _mm256_mul_ps(v1_x, v2_z));
    *n_z = _mm256_sub_ps(_mm256_mul_ps(v1_x, v2_y), _mm256_mul_ps(v1_y, v2_x));
}

void m256_normalize(__m256* x, __m256* y, __m256* z)
{
    __m256 l = _mm256_sqrt_ps(_mm256_add_ps( _mm256_add_ps(
                              _mm256_mul_ps(*x, *x),
                              _mm256_mul_ps(*y, *y)),
                              _mm256_mul_ps(*z, *z)));

    *x = _mm256_div_ps(*x, l);
    *y = _mm256_div_ps(*y, l);
    *z = _mm256_div_ps(*z, l);
}

__m256 m256_dot(__m256 x1, __m256 y1, __m256 z1,
             __m256 x2, __m256 y2, __m256 z2)
{
    __m256 result = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(x1, x2),
                            _mm256_mul_ps(y1, y2)),
                            _mm256_mul_ps(z1, z2));

    return result;
}

__m256 m256_sq_len(__m256 x, __m256 y, __m256 z)
{
    return m256_dot(x, y, z, x, y ,z);
}

#endif // !MUH_WIDE_MATH
