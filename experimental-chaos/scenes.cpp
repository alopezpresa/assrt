#ifndef MUH_SCENES
#define MUH_SCENES

#include <vector>
#include "math.cpp"
#include "render.h"
#include "camera.h"
#include "files.h"

void random_scene(std::vector<RenderObjectID>* list, int amount)
{
    MaterialID ground_material = Material_CreateLambertian(color(0.5, 0.5, 0.5));
    list->push_back(RenderObject_CreateSphere(vec3(0,-1000,0), 1000, ground_material));

    for (int a = -amount; a < amount; a++)
    {
        for (int b = -amount; b < amount; b++)
        {
            float choose_mat = random_float();
            vec3 center(a + 0.9*random_float(), 0.2, b + 0.9*random_float());
            RenderObjectID new_object;

            if ((center - vec3(4, 0.2, 0)).length() > 0.9)
            {
                MaterialID sphere_material;

                if (choose_mat < 0.8)
                {
                    // diffuse
                    color albedo = color::random() * color::random();
                    sphere_material = Material_CreateLambertian(albedo);
                    new_object = RenderObject_CreateSphere(center, 0.2, sphere_material);
                }
                else if (choose_mat < 0.95)
                {
                    // metal
                    color albedo = color::random(0.5, 1);
                    float fuzz = random_float(0, 0.5);
                    sphere_material = Material_CreateMetal(albedo, fuzz);
                    new_object = RenderObject_CreateSphere(center, 0.2, sphere_material);
                }
                else
                {
                    // glass
                    sphere_material = Material_CreateDielectric(1.5);
                    new_object = RenderObject_CreateSphere(center, 0.2, sphere_material);
                }

                list->push_back(new_object);
            }
        }
    }

    MaterialID material1 = Material_CreateDielectric(1.5);
    list->push_back(RenderObject_CreateSphere(vec3(0, 1, 0), 1.0, material1));

    MaterialID material2 = Material_CreateLambertian(color(0.4, 0.2, 0.1));
    list->push_back(RenderObject_CreateSphere(vec3(-4, 1, 0), 1.0, material2));

    MaterialID material3 = Material_CreateMetal(color(0.7, 0.6, 0.5), 0.0);
    list->push_back(RenderObject_CreateSphere(vec3(4, 1, 0), 1.0, material3));
}

RenderObjectID build_scene(camera* cam, float aspect_ratio)
{
    std::vector<RenderObjectID> list;
#if SCENE == SCENE_SIMPLE || SCENE == SCENE_EMPTY || SCENE == SCENE_MESH_SIMPLE || SCENE == SCENE_MESH_FILES
    vec3 lookfrom(0,0,2);
    vec3 lookat(0,0,0);
    vec3 vup(0,1,0);
    float dist_to_focus = 1;
    float aperture = 0.01f;
    float vfov = 45;

    MaterialID material_ground = Material_CreateLambertian(color(0.8f, 0.8f, 0.0f));
    MaterialID material_center = Material_CreateLambertian(color(0.1f, 0.2f, 0.5f));
    MaterialID material_left = Material_CreateDielectric(1.5f);
    MaterialID material_right = Material_CreateMetal(color(0.8f, 0.6f, 0.2f), 0.3f);

#if SCENE == SCENE_SIMPLE
    list.push_back(RenderObject_CreateSphere(vec3( 0.0, -100.5, -1.0), 100.0, material_ground));
    list.push_back(RenderObject_CreateSphere(vec3( 0.0,    0.0, -1.0),   0.5, material_center));
    list.push_back(RenderObject_CreateSphere(vec3(-1.0,    0.0, -1.0),   0.5, material_left));
    list.push_back(RenderObject_CreateSphere(vec3(-1.0,    0.0, -1.0),   -0.45, material_left));
    list.push_back(RenderObject_CreateSphere(vec3( 1.0,    0.0, -1.0),   0.5, material_right));
#elif SCENE == SCENE_MESH_FILES
    vec3 floorVertex1(-50.0f, -0.6f, -50.0f);
    vec3 floorVertex2( 50.0f, -0.6f, -50.0f);
    vec3 floorVertex3( 50.0f, -0.6f,  50.0f);
    vec3 floorVertex4(-50.0f, -0.6f,  50.0f);

    std::vector<triangle> floor_triangles1;
    floor_triangles1.push_back(triangle(floorVertex1, floorVertex4, floorVertex3));
    floor_triangles1.push_back(triangle(floorVertex3, floorVertex2, floorVertex1));
    RenderObjectID floor1 = RenderObject_CreateMesh(floor_triangles1, material_ground);
    list.push_back(floor1);


    /*
    std::vector<triangle> t_rex_mesh_tris = get_obj_file_tris("res/t-rex-simple.obj");
    RenderObjectID t_rex_mesh_id = RenderObject_CreateMesh(t_rex_mesh_tris, material_right);
    list.push_back(t_rex_mesh_id);
    */

    std::vector<triangle> tree_mesh_tris = get_obj_file_tris("res/Tree.obj");
    RenderObjectID tree_mesh_id = RenderObject_CreateMesh(tree_mesh_tris, material_center);
    list.push_back(tree_mesh_id);

    std::vector<triangle> rock_mesh_tris = get_obj_file_tris("res/Rock.obj");
    for (triangle& t : rock_mesh_tris)
    {
        t.a.x += 2;
        t.b.x += 2;
        t.c.x += 2;
    }
    RenderObjectID rock_mesh_id = RenderObject_CreateMesh(rock_mesh_tris, material_right);
    list.push_back(rock_mesh_id);

    std::vector<triangle> log_mesh_tris = get_obj_file_tris("res/Log.obj");
    for (triangle& t : log_mesh_tris)
    {
        t.a.x -= 2;
        t.b.x -= 2;
        t.c.x -= 2;
    }
    RenderObjectID log_mesh_id = RenderObject_CreateMesh(log_mesh_tris, material_center);
    list.push_back(log_mesh_id);

     /*
    std::vector<triangle> cube_mes_tris = get_obj_file_tris("res/cube.obj");
    RenderObjectID cube_mes_id = RenderObject_CreateMesh(cube_mes_tris, material_center);
    list.push_back(cube_mes_id);
    */


#elif SCENE == SCENE_MESH_SIMPLE
    /*
       list.push_back(RenderObject_CreateSphere(vec3( 0.0, -100.5, -1.0), 100.0, material_ground));
    list.push_back(RenderObject_CreateSphere(vec3(1.2, 0.0, .0), 0.35f, material_right));
    list.push_back(RenderObject_CreateSphere(vec3(-1.2, 0.0, .0), 0.35f, material_center));
    */

    vec3 floorVertex1(-50.0f, -0.6f, -50.0f);
    vec3 floorVertex2( 50.0f, -0.6f, -50.0f);
    vec3 floorVertex3( 50.0f, -0.6f,  50.0f);
    vec3 floorVertex4(-50.0f, -0.6f,  50.0f);

    std::vector<triangle> floor_triangles1;
    floor_triangles1.push_back(triangle(floorVertex1, floorVertex4, floorVertex3));
    floor_triangles1.push_back(triangle(floorVertex3, floorVertex2, floorVertex1));
    RenderObjectID floor1 = RenderObject_CreateMesh(floor_triangles1, material_ground);
    list.push_back(floor1);

    vec3 wallVertex1(-3.0f, 1.5f,  -5.0f);
    vec3 wallVertex2( 3.0f, 1.5f,  -5.0f);
    vec3 wallVertex3( 3.0f, 0.3f,  -5.0f);
    vec3 wallVertex4(-3.0f, 0.3f,  -5.0f);

    std::vector<triangle> wall1_triangles;
    wall1_triangles.push_back(triangle(wallVertex3, wallVertex2, wallVertex1));
    RenderObjectID wall1 = RenderObject_CreateMesh(wall1_triangles, material_center);
    list.push_back(wall1);

    std::vector<triangle> wall2_triangles;
    wall2_triangles.push_back(triangle(wallVertex4, wallVertex3, wallVertex1));
    RenderObjectID wall2 = RenderObject_CreateMesh(wall2_triangles, material_right);
    list.push_back(wall2);

    std::vector<MaterialID> random_materials =
    {
        Material_CreateLambertian(color(0.9f, 0.2f, 0.2f)),
        Material_CreateLambertian(color(0.2f, 0.9f, 0.2f)),
        Material_CreateLambertian(color(0.2f, 0.2f, 0.9f)),
        Material_CreateLambertian(color(0.2f, 0.2f, 0.2f)),
#if 0
        Material_CreateDielectric(random_float() * 2.5f),
        Material_CreateDielectric(random_float() * 2.5f),
        Material_CreateDielectric(random_float() * 2.5f),
        Material_CreateDielectric(random_float() * 2.5f),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
#endif
    };

    vec3 vertexA(-0.3f, 0.0f, -0.3);
    vec3 vertexB( 0.3f, 0.0f, -0.3);
    vec3 vertexC( 0.3f, 0.0f,  0.3);
    vec3 vertexD(-0.3f, 0.0f,  0.3);
    vec3 vertexE( 0.0f, 0.5f,  0.0);

    std::vector<vec3> offsets =
    {
#if 1
        vec3(-0.5f,   0.3f, -0.3f),
        vec3( 0.5f,   0.3f, -0.3f),
        vec3(-0.5f,  -0.3f, -0.3f),
        vec3( 0.5f,  -0.3f, -0.3f),
#elif 0
        vec3(0.5f,  1.0f, -0.5f),
        vec3(0.5f,  0.5f, -0.5f),
        vec3(0.5f,  0.0f, -0.5f),
        vec3(0.5f, -0.5f, -0.5f),

        vec3(0.0f,  1.0f, -0.5f),
        vec3(0.0f,  0.5f, -0.5f),
        vec3(0.0f,  0.0f, -0.5f),
        vec3(0.0f, -0.5f, -0.5f),

        vec3(-0.5f,  1.0f, -0.5f),
        vec3(-0.5f,  0.5f, -0.5f),
        vec3(-0.5f,  0.0f, -0.5f),
        vec3(-0.5f, -0.5f, -0.5f),

        vec3(-1.05f,  1.0f, -0.25f),
        vec3(-1.05f,  0.5f, -0.25f),
        vec3(-1.05f,  0.0f, -0.25f),
        vec3(-1.05f, -0.5f, -0.25f),
#elif 0
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
#endif
    };

    for (int i = 0; i < offsets.size(); ++i)
    {
        // Rotate the vertices around the y-axis by 45 degrees
        float angle = (1 * 45.0f) * pi / 180.0f; // convert to radians
        mat3 rotY = mat3(cos(angle), 0, sin(angle), 0, 1, 0, -sin(angle), 0, cos(angle));
        vec3 currentVertexA = rotY * vertexA;
        vec3 currentVertexB = rotY * vertexB;
        vec3 currentVertexC = rotY * vertexC;
        vec3 currentVertexD = rotY * vertexD;
        vec3 currentVertexE = rotY * vertexE;

        vec3 const& pos = offsets[i];

        currentVertexA += pos;
        currentVertexB += pos;
        currentVertexC += pos;
        currentVertexD += pos;
        currentVertexE += pos;

        std::vector<triangle> triangles;
        triangles.push_back(triangle(currentVertexB, currentVertexA, currentVertexE));
        triangles.push_back(triangle(currentVertexC, currentVertexB, currentVertexE));
        triangles.push_back(triangle(currentVertexD, currentVertexC, currentVertexE));
        triangles.push_back(triangle(currentVertexA, currentVertexD, currentVertexE));
        triangles.push_back(triangle(currentVertexA, currentVertexC, currentVertexD));
        triangles.push_back(triangle(currentVertexB, currentVertexC, currentVertexA));

        RenderObjectID new_object = RenderObject_CreateMesh(triangles, random_materials[ i % random_materials.size()]);
        list.push_back(new_object);
    }
#endif

#else
    vec3 lookfrom(-2,1,15);
    vec3 lookat(0,1,0);
    vec3 vup(0,1,0);
    float dist_to_focus = 10;
    float aperture = 0.1f;
    float vfov = 20;

#if SCENE == SCENE_EASY
    random_scene(&list, 6);
#elif SCENE == SCENE_HARD
    random_scene(&list, 11);
#endif
#endif

    *cam = camera(lookfrom, lookat, vup, vfov, aspect_ratio, aperture, dist_to_focus);

#if 0
    return RenderObject_CreateBVHNode(list);
#else
    return RenderObject_CreateList(list);
#endif
}

#endif // !MUH_SCENES
