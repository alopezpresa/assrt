#ifndef MUH_MEMORY
#define MUH_MEMORY

template <typename T>
T* aligned_alloc(size_t alignment, size_t count)
{
    char* mem = new char[count*sizeof(T)/sizeof(char) + (alignment - 1)/sizeof(char)];
    size_t offset = (unsigned long long)mem % alignment;
    T* result = (T*)(mem + (offset/sizeof(char)));

    size_t new_offset = (unsigned long long)result % alignment;
#if 1
    printf("original alignment offset %ld\n", offset);
    printf("   fixed alignment offset %ld\n", new_offset);
#endif
    assert(new_offset == 0);

    return result;
}

#endif // MUH_MEMORY
