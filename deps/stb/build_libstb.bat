@echo off

set LibName=libstb
set OutputLib=%LibName%.lib
set OutputObj=%LibName%.obj

set LinkerFlags= /link /SUBSYSTEM:WINDOWS
set CommonFlags=/nologo /DPLAT_WIN64 -W3

cl.exe /c /O2 /EHsc libstb.cpp /Fo"%OutputObj%" %CommonFlags% %LinkerFlags%
lib.exe /OUT:"%OutputLib%" "%OutputObj%"

del "%OutputObj%"

