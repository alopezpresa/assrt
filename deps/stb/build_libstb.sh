lib_name="libstb"
output_a=$lib_name".a"
output_o=$lib_name".o"

options=""

clang++ -o $output_o -c -Ofast libstb.cpp 

ar ru $output_a $output_o

rm $output_o
