output="raytracer"

libstb="libs/libstb.a"

if [ ! -f "./$libstb" ] ; then
    cd libs
    ./build_libstb.sh
    cd -
fi

options=""
#options=$options" -g"
options=$options" -O3"
options=$options" -mavx2"

project_libs=$libstb""

system_libs=""
system_libs=$system_libs" -lsdl2"
#system_libs=$system_libs" -framework OpenGL"

inlcude=""

clang++ -std=c++14 -stdlib=libc++ $inlcude $system_libs -o $output $options main.cpp $project_libs

