**Huh!?**

After watching a [code review video](https://www.youtube.com/watch?v=mOSirVeP5lo) of a CPU offline raytrace renderer based on the [Ray Tracing in One Weekend](https://raytracing.github.io/) book series and I couldn't resist giving it a try.
The only real objective of this project is to learn something new and code something fun, but I'd love to achieve some level "real time" functionality even if it's at a really low resolution.

**Only tested on Mac because that's what I have in hand**

Windows and Linux support will likely come at some point (and Mac support will be likely dropped)
No clear structure, everything is a mess at the moment.

**ToDo** In no particular order:

- Refactor because it's horrible and it's a mess
- Lights
- Run in GPU
- Use a different window/display solution, maybe move this to OpenGL or Vulkan to be able to use dear imgui and AMD FSR or something like that
- Throw it away and start from scratch.

**Done** but I'm not happy with

- Display in a window
- Make a repo so you stop breaking things and having to guess how to fix them
- Meshes
- Textures
- SIMD, SoA and other CPU optimizations
- Interactive camera
- Cubes/boxes

