#ifndef MUH_FILES_H
#define MUH_FILES_H

#include <vector>
#include "render.h"

enum image_format
{
    image_format_BMP,
    image_format_PNG,
    image_format_JPG,
    image_format_PPM
};

void write_image(char const* path, image_buffer const& buffer, image_format format);

void obj_file_count_vert_and_faces(const char* fileData, int fileSize, int* vertexCount, int* faceCount);

std::vector<triangle> get_obj_file_tris(const char* filepath);

std::vector<MeshID> fully_load_obj_file_things(const char* filepath);

#endif // !MUH_FILES_H
