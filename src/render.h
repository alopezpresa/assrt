#ifndef MUH_RENDER_H
#define MUH_RENDER_H

#include <math.h>
#include "math.cpp"

typedef int MaterialID;
typedef int TextureID;
typedef int MeshID;
typedef int RenderNodeID;

union color4
{
    struct
    {
        float r;
        float g;
        float b;
        float a;
    };

    float e[4];

    color4()
    {
    }

    color4(float r, float g, float b, float a)
    : r(r), g(g), b(b), a(a)
    { }

    color4(color const& c)
    {
        r = c.r;
        g = c.g;
        b = c.b;
        a = 1.0f;
    }

    inline color4& operator+=(color4 const& b)
    {
        this->r += b.r;
        this->g += b.g;
        this->b += b.b;
        this->a += b.a;
        return *this;
    }
};

struct image_buffer
{
    image_buffer(int width, int height, int samples_per_pixel)
    {
        this->width = width;
        this->height = height;
        this->samples_per_pixel = samples_per_pixel;

        pixels = new color4[height * width];
    }

    ~image_buffer()
    {
        delete [] pixels;
    }

    int width;
    int height;
    int samples_per_pixel;
    color4* pixels;
};

union bytes_color
{
    struct
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    };

    uint8_t e[3];

    bytes_color(color4 color, int samples_per_pixel)
    {
        // Divide the color by the number of samples and gamma-correct for gamma=2.0.
        float scale = 1.0 / samples_per_pixel;
        r = (uint8_t)(clamp(sqrt(scale * color.r), 0.0f, 0.999f) * 256);
        g = (uint8_t)(clamp(sqrt(scale * color.g), 0.0f, 0.999f) * 256);
        b = (uint8_t)(clamp(sqrt(scale * color.b), 0.0f, 0.999f) * 256);
    }

    bytes_color(color color, int samples_per_pixel)
    {
        // Divide the color by the number of samples and gamma-correct for gamma=2.0.
        float scale = 1.0 / samples_per_pixel;
        r = (uint8_t)(clamp(sqrt(scale * color.r), 0.0f, 0.999f) * 256);
        g = (uint8_t)(clamp(sqrt(scale * color.g), 0.0f, 0.999f) * 256);
        b = (uint8_t)(clamp(sqrt(scale * color.b), 0.0f, 0.999f) * 256);
    }
};

struct hit_record
{
    vec3 p;
    vec3 normal;
    float t, u, v;
    MaterialID material_id;

    hit_record() : t(-1) { }
};

enum MaterialType
{
    MaterialType_Lambertian,
    MaterialType_Metal,
    MaterialType_Dielectric
};

struct Texture
{
    int width;
    int height;

    union {
        color4* pixels;
        float* data;
    };
};

TextureID Texture_Load(const char* filepath);
Texture* Texture_Get(TextureID id);
color Texture_Sample(TextureID id, float u, float v);
void Texture_Free(TextureID id);

struct Material
{
    MaterialType type;
    color albedo;
    TextureID diffuse_map;

    union
    {
        float fuzz;
        float ir; // Index of Refraction
    };

    Material(MaterialType type) : type(type), diffuse_map(-1) { }

};

MaterialID Material_CreateLambertian(color const& a);
MaterialID Material_CreateMetal(color const& a, float f);
MaterialID Material_CreateDielectric(float index_of_refraction);
MaterialID Material_CreateDielectric(float index_of_refraction, color const& a);
Material* Material_Get(MaterialID id);

struct triangle
{
    union 
    {
        vec3 components[3];

        struct {
            vec3 a, b, c;
        };
    };

    union 
    {
        vec3 texture_coords[3];

        struct {
            vec3 ta, tb, tc;
        };
    };

    triangle(vec3 a, vec3 b, vec3 c, vec3 ta, vec3 tb, vec3 tc)
        : a(a), b(b), c(c), ta(ta), tb(tb), tc(tc)
    { }

    bool hit(ray const& r, float t_min, float t_max, hit_record* result) const
    {
        vec3 v0v1 = b - a;
        vec3 v0v2 = c - a;
        vec3 pvec = cross(r.direction, v0v2);
        float det = dot(v0v1, pvec);
        if (det < epsilon) return false;

        float invDet = 1 / det;
        vec3 tvec = r.origin - a;
        float u = dot(tvec, pvec) * invDet;
        if (u < 0 || u > 1) return false;

        vec3 qvec = cross(tvec, v0v1);
        float v = dot(r.direction, qvec) * invDet;
        if (v < 0 || u + v > 1) return false;

        float t = dot(v0v2, qvec) * invDet;
        if (t < t_min || t > t_max) return false;

        result->t = t;
        result->p = r.at(t);
        result->u = ta.x; // TODO: "lerp" the UV based on the distance to each vertex
        result->v = ta.y;
        result->normal = cross(v0v1, v0v2);
        return true;
    }
};

struct Mesh
{
    MaterialID material_id;
    aabb bounding_box;

    std::vector<triangle> triangles;
};

Mesh* Mesh_Get(MeshID id);
MeshID Mesh_Create(std::vector<triangle> const& triangles, MaterialID material_id);

void InitTrianglesThing();
void ResetTrianglesThing();

struct RenderNode
{
    RenderNode()
     : is_leaf(false)
     , material_id(-1)
    {}

    MaterialID material_id;
    bool is_leaf;
    aabb bounding_box;

    union
    {
        struct
        {
            RenderNodeID left_id;
            RenderNodeID right_id;
        } childs;

        struct
        {
            int begin;
            int end;
        } triangles;
    };

};

RenderNode* RenderNode_Get(RenderNodeID id);
void RenderNode_ClearAll();
RenderNodeID RenderNode_CreateBVHNode(std::vector<MeshID> const& meshes);

#endif // !MUH_RENDER_H
