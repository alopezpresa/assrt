#ifndef MUH_RENDER_CPP
#define MUH_RENDER_CPP

#include <cassert>
#include <ctype.h>
#include <cstdio>
#include <immintrin.h>
#include <math.h>
#include <smmintrin.h>
#include <unordered_map>
#include <xmmintrin.h>
#include "math.cpp"
#include "wmath.cpp"
#include "render.h"
#include "memory.cpp"
#include "stb/stb_image.h"

// tired of lsp errors
#ifndef SIMD_FLOAT_WIDTH
#define SIMD_FLOAT_WIDTH 8 
#endif // !SIMD_FLOAT_WIDTH

std::unordered_map<std::string, TextureID> textures_by_name;
std::vector<Texture> textures;

TextureID Texture_Load(const char* filepath)
{
    TextureID texture_id;
    auto texture_data = textures_by_name.find(filepath);

    if (texture_data == textures_by_name.end())
    {
        int width, height, channels;
        unsigned char *img = stbi_load(filepath, &width, &height, &channels, 4);

        if (img == NULL)
        {
            printf("Error in loading the image '%s'\n", filepath);
            assert(false);
        }

        int texture_size = width*height;

        Texture t;
        t.width = width;
        t.height = height;
        t.pixels = new color4[texture_size];

        for (int i = 0; i < texture_size*4; ++i)
        {
            t.data[i] = img[i] / 255.0f;
        }

        stbi_image_free(img);

        textures.push_back(t);

        texture_id = textures.size()-1;
        textures_by_name[filepath] = texture_id;
        printf("loaded new loaded texture: %s\n", filepath);
    }
    else
    {
        texture_id = texture_data->second;
        printf("reusing loaded texture: %s\n", filepath);
    }

    return texture_id;
}

Texture* Texture_Get(TextureID id)
{
    return &textures[id];
}

color Texture_Sample(TextureID id, float u, float v)
{
    Texture* t = Texture_Get(id);
    int x = u * t->width;
    int y = t->height - v * t->height;
    int i = y * t->width + x;
    color4 texture_color = t->pixels[i];
    return color(texture_color.r, texture_color.g, texture_color.b);
}

void Texture_Free(TextureID id)
{
    // i can't really delete it because it will break the array...
    Texture* t = Texture_Get(id);
    delete [] t->pixels;
    t->pixels = NULL;
    t->width = 0;
    t->height = 0;
}

std::vector<Material> materials;

Material* Material_Get(MaterialID id)
{
    assert(0 <= id && id < materials.size());
    return &materials[id];
}

MaterialID Material_Create(MaterialType type)
{
    Material new_material(type);
    materials.push_back(new_material);
    MaterialID new_material_id = materials.size() - 1;
    return new_material_id;
}

MaterialID Material_CreateLambertian(color const& a)
{
    MaterialID new_material_id = Material_Create(MaterialType_Lambertian);

    materials[new_material_id].albedo = a;

    return new_material_id;
}

MaterialID Material_CreateMetal(color const& a, float f)
{
    MaterialID new_material_id = Material_Create(MaterialType_Metal);

    materials[new_material_id].albedo = a;
    materials[new_material_id].fuzz = clamp(f, 0, 1);

    return new_material_id;
}

MaterialID Material_CreateDielectric(float index_of_refraction)
{
    return Material_CreateDielectric(index_of_refraction, color(1, 1, 1));
}

MaterialID Material_CreateDielectric(float index_of_refraction, color const& a)
{
    MaterialID new_material_id = Material_Create(MaterialType_Dielectric);

    materials[new_material_id].ir = index_of_refraction;
    materials[new_material_id].albedo = a;

    return new_material_id;
}

inline float reflectance(float cosine, float ref_idx)
{
    // Use Schlick's approximation for reflectance.
    float r0 = (1.0f-ref_idx) / (1.0f+ref_idx);
    r0 = r0*r0;
    return r0 + (1.0f-r0) * pow((1.0f - cosine), 5.0f);
}

bool Material_ScatterRayHit(ray const& r_in, hit_record const& hit, color* attenuation, ray* scattered)
{
    Material* material = Material_Get(hit.material_id);

    switch (material->type)
    {
        case MaterialType_Lambertian:
            {
                // NOTE:
                // Other scattering tecnics for mate lambertian materials:
                // vec3 target = hit.p + random_in_hemisphere(hit.normal);
                // using random_in_hemisphere or random_in_unit_sphere will work, but 
                // it's not a proper aproximation and results in darker, less acurate results
                // It should be faster however because we're saving lenght and other
                // calculations.

                vec3 scatter_direction = hit.normal + random_unit_vector();

                if (scatter_direction.near_zero())
                {
                    scatter_direction = hit.normal;
                }

                scattered->set(hit.p, scatter_direction);

                if (material->diffuse_map >= 0)
                {
                    *attenuation = Texture_Sample(material->diffuse_map, hit.u, hit.v);
                }
                else
                {
                    *attenuation = material->albedo;
                }

                return true;
            }
            break;

        case MaterialType_Metal:
            {
                vec3 reflected = reflect(unit_vector(r_in.direction), hit.normal);
                scattered->set(hit.p, reflected + material->fuzz*random_in_unit_sphere());
                *attenuation = material->albedo;
                return (dot(scattered->direction, hit.normal) > 0);
            }
            break;

        case MaterialType_Dielectric:
            {
                *attenuation = material->albedo;

                float refraction_ratio = 1.0f/material->ir;

                vec3 unit_direction = unit_vector(r_in.direction);

                float cos_theta = fmin(dot(-unit_direction, hit.normal), 1.0f);
                float sin_theta = sqrt(1.0f - cos_theta*cos_theta);

                bool cannot_refract = refraction_ratio * sin_theta > 1.0f;
                vec3 direction;

                if (cannot_refract || reflectance(cos_theta, refraction_ratio) > random_float())
                {
                    direction = reflect(unit_direction, hit.normal);
                }
                else
                {
                    direction = refract(unit_direction, hit.normal, refraction_ratio);
                }

                scattered->set(hit.p, direction);

                return true;
            }
            break;
    }
}

std::vector<Mesh> meshes;
Mesh* Mesh_Get(MeshID id)
{
    return &meshes[id];
}

MeshID Mesh_Create(std::vector<triangle> const& triangles, MaterialID material_id)
{
    Mesh new_mesh;
    new_mesh.material_id = material_id;
    new_mesh.triangles = triangles;

    for (triangle const& t : triangles)
    {
        new_mesh.bounding_box.expand(t.a);
        new_mesh.bounding_box.expand(t.b);
        new_mesh.bounding_box.expand(t.c);
    }

    meshes.push_back(new_mesh);
    return meshes.size()-1;
}

std::vector<RenderNode> render_nodes;

RenderNode* RenderNode_Get(RenderNodeID id)
{
    return &render_nodes[id];
}

void RenderNode_ClearAll()
{
    render_nodes.clear();
}

inline bool box_compare(triangle a, triangle b, int axis)
{
    float min_a = fmin(fmin(a.a[axis], a.b[axis]), a.c[axis]);
    float min_b = fmin(fmin(b.a[axis], b.b[axis]), b.c[axis]);
    return min_a < min_b;
}

inline bool box_compare(RenderNodeID a, RenderNodeID b, int axis)
{
    aabb const& box_a = RenderNode_Get(a)->bounding_box;
    aabb const& box_b = RenderNode_Get(b)->bounding_box;

    if (!box_a.is_valid() || !box_b.is_valid())
    {
#if !PER_FRAME_BVH_REBUILD
        printf("box_compare error. No bounding box in bvh_node constructor.\n");
#endif
    }

    return box_a.minimum.e[axis] < box_b.minimum.e[axis];
}

template <typename T>
inline bool box_x_compare (T a, T b) {
    return box_compare(a, b, 0);
}

template <typename T>
inline bool box_y_compare (T a, T b) {
    return box_compare(a, b, 1);
}

template <typename T>
inline bool box_z_compare (T a, T b) {
    return box_compare(a, b, 2);
}

template <typename T>
inline void split(std::vector<T> all, std::vector<T>* first, std::vector<T>* second)
{
    int axis = random_int(0,3);
    auto comparator =  (axis == 0) ? box_x_compare<T>
                    : ((axis == 1) ? box_y_compare<T>
                                   : box_z_compare<T>);

    std::sort(all.begin(), all.end(), comparator);
    auto half_size = all.size() / 2;
    auto mid = all.begin() + half_size;

    first->clear();
    first->reserve(half_size);
    first->insert(first->begin(), all.begin(), mid);

    second->clear();
    second->reserve(half_size);
    second->insert(second->begin(), mid, all.end());
}

RenderNodeID RenderNode_CreateBVHNode()
{
    render_nodes.push_back(RenderNode());
    RenderNodeID node_id = render_nodes.size()-1;
    return node_id;
}

void RenderNode_ExpandBoundingsWithChilds(RenderNodeID node_id)
{
    aabb const& box_left = RenderNode_Get(RenderNode_Get(node_id)->childs.left_id)->bounding_box;
    aabb const& box_right = RenderNode_Get(RenderNode_Get(node_id)->childs.right_id)->bounding_box;

    if (!box_left.is_valid() || !box_right.is_valid())
    {
        printf("RenderNode_CreateNode - No bounding box in bvh_node constructor.\n");
    }

    RenderNode_Get(node_id)->bounding_box = surrounding_box(box_left, box_right);
}

#define TRI_SCENE_LIMIT (1024*16)
#define VEC_COORDS 3 // x, y, z 
#define TRI_COMPS 3 // a, b-a, c-a

static float* tris_data = NULL;
static float* texture_coords_data = NULL;
static int tri_data_current_max = 0;

void InitTrianglesThing()
{
    tris_data = aligned_alloc<float>(32, TRI_SCENE_LIMIT * TRI_COMPS * VEC_COORDS);
    texture_coords_data = aligned_alloc<float>(32, TRI_SCENE_LIMIT * TRI_COMPS * VEC_COORDS);
}

void ResetTrianglesThing()
{
    tri_data_current_max = 0;
}

void RenderNode_FillWithTriangles(RenderNodeID node_id, std::vector<triangle> triangles, MaterialID material_id)
{
    // NOTE/TODO arbitrary limit, thing something simd friendly later
    if (triangles.size() <= SIMD_FLOAT_WIDTH * 16)
    {
        RenderNode* node = RenderNode_Get(node_id);
        node->is_leaf = true;
        node->childs.left_id = node->childs.right_id = -1;
        node->material_id = material_id;
        node->triangles.begin = tri_data_current_max;

#if !PER_FRAME_BVH_REBUILD
        printf("start node %d, %zd real triangles starting at %d\n", node_id, triangles.size(), tri_data_current_max);
#endif
        
        for (int i = 0; i < triangles.size(); i += SIMD_FLOAT_WIDTH)
        {
            for (int j = 0; j < SIMD_FLOAT_WIDTH; ++j)
            {
                int tri_idx = i + j;
                if (tri_idx < triangles.size())
                {
                    triangle const& t = triangles[tri_idx];
                    node->bounding_box.expand(t.a);
                    node->bounding_box.expand(t.b);
                    node->bounding_box.expand(t.c);
                }
            }

            for (int comp = 0; comp < TRI_COMPS; ++comp)
            {
                for (int coord = 0; coord < VEC_COORDS; ++coord)
                {
                    for (int j = 0; j < SIMD_FLOAT_WIDTH; ++j)
                    {
                        int tri_idx = i + j;
                        float value = 0; 
                        float texture_value = 0; 

                        if (tri_idx < triangles.size())
                        {
                            triangle const& t = triangles[tri_idx];
                            switch (comp)
                            {
                                case 0:
                                    {
                                        value = t.a.e[coord];
                                        texture_value = t.ta.e[coord];
                                    }
                                    break;
                                case 1:
                                    {
                                        value = (t.b - t.a).e[coord];
                                        texture_value = t.tb.e[coord];
                                    }
                                    break;
                                case 2:
                                    {
                                        value = (t.c - t.a).e[coord];
                                        texture_value = t.tc.e[coord];
                                    }
                                    break;
                            }
                        }

                        tris_data[tri_data_current_max] = value;
                        texture_coords_data[tri_data_current_max] = texture_value;

                        ++tri_data_current_max;
                    }
                }
            }
        }

        node->triangles.end = tri_data_current_max;

#if !PER_FRAME_BVH_REBUILD
        printf("end node %d, %zd real triangles ending at %d\n", node_id, triangles.size(), tri_data_current_max);
#endif
    }
    else
    {
#if !PER_FRAME_BVH_REBUILD
        printf("split node %d, %zd real triangles to split\n", node_id, triangles.size());
#endif
        std::vector<triangle> first_half, second_half;
        split(triangles, &first_half, &second_half);
        RenderNode_Get(node_id)->childs.left_id = RenderNode_CreateBVHNode();
        RenderNode_Get(node_id)->childs.right_id = RenderNode_CreateBVHNode();
        RenderNode_FillWithTriangles(RenderNode_Get(node_id)->childs.left_id, first_half, material_id);
        RenderNode_FillWithTriangles(RenderNode_Get(node_id)->childs.right_id, second_half, material_id);
        RenderNode_ExpandBoundingsWithChilds(node_id);
    }
}

RenderNodeID RenderNode_CreateBVHNode(std::vector<MeshID> const& meshes)
{
    render_nodes.push_back(RenderNode());
    RenderNodeID node_id = render_nodes.size()-1;

    if (meshes.size() == 1)
    {
        Mesh* mesh = Mesh_Get(meshes[0]);
        RenderNode_FillWithTriangles(node_id, mesh->triangles, mesh->material_id);
    }
    else
    {
        std::vector<RenderNodeID> first_half, second_half;
        split(meshes, &first_half, &second_half);
        RenderNode_Get(node_id)->childs.left_id = RenderNode_CreateBVHNode(first_half);
        RenderNode_Get(node_id)->childs.right_id = RenderNode_CreateBVHNode(second_half);
        RenderNode_ExpandBoundingsWithChilds(node_id);
    }


    return node_id;
}

bool RenderNode_RayHit(RenderNodeID node_id, ray const& r, float t_min, float t_max, hit_record* result)
{
    RenderNode* node = RenderNode_Get(node_id);

    if (node->is_leaf)
    {
        // 'wasted' work per mesh?
        __m256 w_ray_dir_x = _mm256_set1_ps(r.direction.x);
        __m256 w_ray_dir_y = _mm256_set1_ps(r.direction.y);
        __m256 w_ray_dir_z = _mm256_set1_ps(r.direction.z);

        __m256 w_ray_orig_x = _mm256_set1_ps(r.origin.x);
        __m256 w_ray_orig_y = _mm256_set1_ps(r.origin.y);
        __m256 w_ray_orig_z = _mm256_set1_ps(r.origin.z);

        __m256 w_t_min = _mm256_set1_ps(t_min);
        __m256 w_t_max  = _mm256_set1_ps(t_max);
        // --------

        // current best values could be carried over to other meshes?
        __m256 w_best_t = w_infinity;
        __m256 w_best_material = _mm256_set1_ps(-1.0f);
        __m256 material_ids = _mm256_set1_ps((float)node->material_id);
        __m256 w_best_n_x;
        __m256 w_best_n_y;
        __m256 w_best_n_z;
        __m256 w_best_u;
        __m256 w_best_v;

        int tri_begin = node->triangles.begin;
        int tri_end = node->triangles.end;
        int inc = SIMD_FLOAT_WIDTH * TRI_COMPS * VEC_COORDS;

#if 0
        printf("start node render, %d triangles starting at %d\n", tri_end-tri_begin, node->triangles.begin);
#endif

        for (int i = tri_begin; i < tri_end; i += inc)
        {
#if 0
            printf(" > render triangle batch starting at %d\n", i);
            printf("   > offsets");
            for (int offset = 0; offset < TRI_COMPS * VEC_COORDS; ++offset)
                printf(", %d", SIMD_FLOAT_WIDTH *  offset);
            printf(".\n");
#endif
            __m256 v0_x = _mm256_load_ps(&tris_data[i + SIMD_FLOAT_WIDTH *  0]);
            __m256 v0_y = _mm256_load_ps(&tris_data[i + SIMD_FLOAT_WIDTH *  1]);
            __m256 v0_z = _mm256_load_ps(&tris_data[i + SIMD_FLOAT_WIDTH *  2]);

            __m256 v0v1_x = _mm256_load_ps(&tris_data[i + SIMD_FLOAT_WIDTH *  3]);
            __m256 v0v1_y = _mm256_load_ps(&tris_data[i + SIMD_FLOAT_WIDTH *  4]);
            __m256 v0v1_z = _mm256_load_ps(&tris_data[i + SIMD_FLOAT_WIDTH *  5]);

            __m256 v0v2_x = _mm256_load_ps(&tris_data[i + SIMD_FLOAT_WIDTH *  6]);
            __m256 v0v2_y = _mm256_load_ps(&tris_data[i + SIMD_FLOAT_WIDTH *  7]);
            __m256 v0v2_z = _mm256_load_ps(&tris_data[i + SIMD_FLOAT_WIDTH *  8]);

            __m256 n_x = _mm256_sub_ps(_mm256_mul_ps(v0v1_y, v0v2_z), _mm256_mul_ps(v0v1_z, v0v2_y));
            __m256 n_y = _mm256_sub_ps(_mm256_mul_ps(v0v1_z, v0v2_x), _mm256_mul_ps(v0v1_x, v0v2_z));
            __m256 n_z = _mm256_sub_ps(_mm256_mul_ps(v0v1_x, v0v2_y), _mm256_mul_ps(v0v1_y, v0v2_x));

            __m256 pvec_x = _mm256_sub_ps(_mm256_mul_ps(w_ray_dir_y, v0v2_z),
                                          _mm256_mul_ps(w_ray_dir_z, v0v2_y));
            __m256 pvec_y = _mm256_sub_ps(_mm256_mul_ps(w_ray_dir_z, v0v2_x),
                                          _mm256_mul_ps(w_ray_dir_x, v0v2_z));
            __m256 pvec_z = _mm256_sub_ps(_mm256_mul_ps(w_ray_dir_x, v0v2_y),
                                          _mm256_mul_ps(w_ray_dir_y, v0v2_x));

            __m256 det = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(v0v1_x, pvec_x),
                            _mm256_mul_ps(v0v1_y, pvec_y)),
                            _mm256_mul_ps(v0v1_z, pvec_z));

            __m256 invDet = _mm256_div_ps(w_one, det);

            __m256 tvec_x = _mm256_sub_ps(w_ray_orig_x, v0_x);
            __m256 tvec_y = _mm256_sub_ps(w_ray_orig_y, v0_y);
            __m256 tvec_z = _mm256_sub_ps(w_ray_orig_z, v0_z);

            __m256 t_p_dot = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(tvec_x, pvec_x),
                            _mm256_mul_ps(tvec_y, pvec_y)),
                            _mm256_mul_ps(tvec_z, pvec_z));

            __m256 u = _mm256_mul_ps(t_p_dot, invDet);

            __m256 qvec_x = _mm256_sub_ps(_mm256_mul_ps(tvec_y, v0v1_z),
                                          _mm256_mul_ps(tvec_z, v0v1_y));
            __m256 qvec_y = _mm256_sub_ps(_mm256_mul_ps(tvec_z, v0v1_x),
                                          _mm256_mul_ps(tvec_x, v0v1_z));
            __m256 qvec_z = _mm256_sub_ps(_mm256_mul_ps(tvec_x, v0v1_y),
                                          _mm256_mul_ps(tvec_y, v0v1_x));

            __m256 dir_q_dot = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(w_ray_dir_x, qvec_x),
                            _mm256_mul_ps(w_ray_dir_y, qvec_y)),
                            _mm256_mul_ps(w_ray_dir_z, qvec_z));
            __m256 v = _mm256_mul_ps(dir_q_dot, invDet);

            __m256 v0v2_q_dot = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(v0v2_x, qvec_x),
                            _mm256_mul_ps(v0v2_y, qvec_y)),
                            _mm256_mul_ps(v0v2_z, qvec_z));

            __m256 current_t = _mm256_mul_ps(v0v2_q_dot, invDet);

            __m256 u_plus_v = _mm256_add_ps(u, v);
            __m256 better_hits_mask = _mm256_cmp_ps(det, w_epsilon, _CMP_GE_OQ);
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u, w_zero, _CMP_GE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u, w_one, _CMP_LE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(v, w_zero, _CMP_GE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(u_plus_v, w_one, _CMP_LE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_t_min, _CMP_GE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_t_max, _CMP_LE_OQ));
            better_hits_mask = _mm256_and_ps(better_hits_mask, _mm256_cmp_ps(current_t, w_best_t, _CMP_LT_OQ));

            w_best_material = _mm256_blendv_ps(w_best_material, material_ids, better_hits_mask);
            w_best_t        = _mm256_blendv_ps(w_best_t       , current_t   , better_hits_mask);
            w_best_n_x      = _mm256_blendv_ps(w_best_n_x     , n_x         , better_hits_mask);
            w_best_n_y      = _mm256_blendv_ps(w_best_n_y     , n_y         , better_hits_mask);
            w_best_n_z      = _mm256_blendv_ps(w_best_n_z     , n_z         , better_hits_mask);

            __m256 vt0_u = _mm256_load_ps(&texture_coords_data[i + SIMD_FLOAT_WIDTH *  0]);
            __m256 vt0_v = _mm256_load_ps(&texture_coords_data[i + SIMD_FLOAT_WIDTH *  1]);

            __m256 vt1_u = _mm256_load_ps(&texture_coords_data[i + SIMD_FLOAT_WIDTH *  3]); // skip the unused 'w' value
            __m256 vt1_v = _mm256_load_ps(&texture_coords_data[i + SIMD_FLOAT_WIDTH *  4]);

            __m256 vt2_u = _mm256_load_ps(&texture_coords_data[i + SIMD_FLOAT_WIDTH *  6]); // skip the unused 'w' value
            __m256 vt2_v = _mm256_load_ps(&texture_coords_data[i + SIMD_FLOAT_WIDTH *  7]);

            __m256 v0p_x = _mm256_sub_ps(_mm256_add_ps(w_ray_orig_x, _mm256_mul_ps(w_ray_dir_x, current_t)), v0_x);
            __m256 v0p_y = _mm256_sub_ps(_mm256_add_ps(w_ray_orig_y, _mm256_mul_ps(w_ray_dir_y, current_t)), v0_y);
            __m256 v0p_z = _mm256_sub_ps(_mm256_add_ps(w_ray_orig_z, _mm256_mul_ps(w_ray_dir_z, current_t)), v0_z);

            __m256 vCrossW_x = _mm256_sub_ps(_mm256_mul_ps(v0v2_y, v0p_z), _mm256_mul_ps(v0v2_z, v0p_y));
            __m256 vCrossW_y = _mm256_sub_ps(_mm256_mul_ps(v0v2_z, v0p_x), _mm256_mul_ps(v0v2_x, v0p_z));
            __m256 vCrossW_z = _mm256_sub_ps(_mm256_mul_ps(v0v2_x, v0p_y), _mm256_mul_ps(v0v2_y, v0p_x));

            __m256 uCrossW_x = _mm256_sub_ps(_mm256_mul_ps(v0v1_y, v0p_z), _mm256_mul_ps(v0v1_z, v0p_y));
            __m256 uCrossW_y = _mm256_sub_ps(_mm256_mul_ps(v0v1_z, v0p_x), _mm256_mul_ps(v0v1_x, v0p_z));
            __m256 uCrossW_z = _mm256_sub_ps(_mm256_mul_ps(v0v1_x, v0p_y), _mm256_mul_ps(v0v1_y, v0p_x));

            __m256 uCrossV_x = _mm256_sub_ps(_mm256_mul_ps(v0v1_y, v0v2_z), _mm256_mul_ps(v0v1_z, v0v2_y));
            __m256 uCrossV_y = _mm256_sub_ps(_mm256_mul_ps(v0v1_z, v0v2_x), _mm256_mul_ps(v0v1_x, v0v2_z));
            __m256 uCrossV_z = _mm256_sub_ps(_mm256_mul_ps(v0v1_x, v0v2_y), _mm256_mul_ps(v0v1_y, v0v2_x));

            __m256 denom = _mm256_sqrt_ps(_mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(uCrossV_x, uCrossV_x),
                            _mm256_mul_ps(uCrossV_y, uCrossV_y)),
                        _mm256_mul_ps(uCrossV_z, uCrossV_z)));

            __m256 b1 = _mm256_div_ps(_mm256_sqrt_ps(_mm256_add_ps(_mm256_add_ps(
                                _mm256_mul_ps(vCrossW_x, vCrossW_x),
                                _mm256_mul_ps(vCrossW_y, vCrossW_y)),
                            _mm256_mul_ps(vCrossW_z, vCrossW_z))),
                    denom);

            __m256 b2 = _mm256_div_ps(_mm256_sqrt_ps(_mm256_add_ps(_mm256_add_ps(
                                _mm256_mul_ps(uCrossW_x, uCrossW_x),
                                _mm256_mul_ps(uCrossW_y, uCrossW_y)),
                                _mm256_mul_ps(uCrossW_z, uCrossW_z))),
                    denom);

            /*
            const Vector v0v1 = v1 - v0;
            const Vector v0v2 = v2 - v0;
            const Vector v0p = p - v0;

            const Vector vCrossW = Cross(v0v2, v0p);
            const Vector uCrossW = Cross(v0v1, v0p);
            const Vector uCrossV = Cross(v0v1, v0v2);

            const float denom = uCrossV.Length();
            const float b1 = vCrossW.Length() / denom;
            const float b2 = uCrossW.Length() / denom;

            const float b0 = 1.f - b1 - b2;
            const u = b0 * vt0_u + b1 * vt1_u + b2 * vt2_u;
            const v = b0 * vt0_v + b1 * vt1_v + b2 * vt2_v;
            */

            __m256 b0 = _mm256_sub_ps(w_one, _mm256_add_ps(b1, b2));

            __m256 t_u = _mm256_add_ps(_mm256_add_ps(
                        _mm256_mul_ps(b0, vt0_u),
                        _mm256_mul_ps(b1, vt1_u)),
                        _mm256_mul_ps(b2, vt2_u));

            __m256 t_v = _mm256_add_ps(_mm256_add_ps(
                        _mm256_mul_ps(b0, vt0_v),
                        _mm256_mul_ps(b1, vt1_v)),
                        _mm256_mul_ps(b2, vt2_v));

            w_best_u = _mm256_blendv_ps(w_best_u, t_u, better_hits_mask);
            w_best_v = _mm256_blendv_ps(w_best_v, t_v, better_hits_mask);
        }

        // Did we get any material? If so, then we hit at least one triangle and
        // we need to find the best hit and process it
        __m256 cmp = _mm256_cmp_ps(w_best_material, w_zero, _CMP_GE_OQ);
        int mask = _mm256_movemask_ps(cmp);
        bool did_hit = mask != 0;

        if (did_hit)
        {
            int best_index = -1;

            for (int hit_index = 0; hit_index < SIMD_FLOAT_WIDTH; ++hit_index)
            {
                if (w_best_material[hit_index] >= 0.0f && w_best_t[hit_index] > t_min && (best_index < 0 || w_best_t[hit_index] < w_best_t[best_index]))
                {
                    best_index = hit_index;
                }

#if 0
                printf("hit info %d :\n\tt: %0.2f\n\tm: %d\n", hit_index, 
                        w_best_t[hit_index], (int)w_best_material[hit_index]);
#endif
            }

            result->t = w_best_t[best_index];
            result->u = clamp(w_best_u[best_index], 0.0f, 1.0f);
            result->v = clamp(w_best_v[best_index], 0.0f, 1.0f);
            result->normal = vec3(w_best_n_x[best_index],
                                  w_best_n_y[best_index],
                                  w_best_n_z[best_index]);
            result->normal.normalize();
            result->p = r.at(result->t);
            result->material_id = (int)w_best_material[best_index];


#if 0
            printf("BEST HIT info %d:\n\tt: %0.2f\n\tm: %d\n\tn: %s\tl: %0.2f\n", best_index, 
                    w_best_t[best_index], (int)w_best_material[best_index], result->normal.c_str(), result->normal.length());

            if (Material_Get(result->material_id)->diffuse_map >= 0)
            {
                printf("hit uv: %f, %f\n", result->u, result->v);
            }
#endif
        }

        return did_hit;
    }
    else
    {
        if (!node->bounding_box.hit(r, t_min, t_max))
        {
            return false;
        }

        bool did_hit_left = RenderNode_RayHit(node->childs.left_id, r, t_min, t_max, result);

        if (node->childs.left_id != node->childs.right_id)
        {
            bool did_hit_right = RenderNode_RayHit(node->childs.right_id, r, t_min, did_hit_left? result->t : t_max, result);
            return did_hit_left || did_hit_right;
        }
        else
        {
            return did_hit_left;
        }
    }
}

color skybox_color(vec3 direction)
{
    vec3 unit_direction = unit_vector(direction);
    float t = 0.5f * (unit_direction.y + 1.0f);
    return lerp(color(1.0f, 1.0f, 1.0f), color(0.7f, 0.8f, 1.0f), t);
}

color ray_color(ray const& original_ray, RenderNodeID world, int depth)
{
    ray current_ray(original_ray);
    hit_record hit;
    color result_color(0.8f, 0.8f, 0.9f);
    float max_distance = 2500.0f;
    float min_distance = 0.1f;

    for (int i = 0; i < depth; ++i)
    {
        bool did_hit = RenderNode_RayHit(world, current_ray, min_distance, max_distance, &hit);

        //exit(10);

        if (did_hit && hit.t < max_distance * 0.9f)
        {
            ray scattered;
            color attenuation;
            if (Material_ScatterRayHit(current_ray, hit, &attenuation, &scattered))
            {
                float t = (hit.t-min_distance) / (max_distance-min_distance);
                current_ray = scattered;
                //result_color *= lerp(attenuation, skybox_color(current_ray.direction), t*t);
                result_color *= attenuation;
            }
            else
            {
                //printf(" > WARNING: Material didn't scattered. What does this mean?\n");
                result_color *= color::zero();
                break;
            }
        }
        else
        {
            result_color *= skybox_color(current_ray.direction);
            break;
        }
    }

    return result_color;
}

#endif // !MUH_RENDER_CPP
