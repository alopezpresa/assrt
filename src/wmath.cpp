#ifndef MUH_WIDE_MATH
#define MUH_WIDE_MATH

#include <limits>
#include <math.h>
#include <random>
#include <stdint.h>
#include <immintrin.h>
#include "math.cpp"

// Constants
static const __m256 w_zero = _mm256_setzero_ps();
static const __m256 w_epsilon = _mm256_set1_ps(epsilon);
static const __m256 w_one = _mm256_set1_ps(1.0f);
static const __m256 w_infinity = _mm256_set1_ps(infinity);

void m256_cross(__m256 v1_x, __m256 v1_y, __m256 v1_z,
             __m256 v2_x, __m256 v2_y, __m256 v2_z,
             __m256* n_x, __m256* n_y, __m256* n_z)
{
    *n_x = _mm256_sub_ps(_mm256_mul_ps(v1_y, v2_z), _mm256_mul_ps(v1_z, v2_y));
    *n_y = _mm256_sub_ps(_mm256_mul_ps(v1_z, v2_x), _mm256_mul_ps(v1_x, v2_z));
    *n_z = _mm256_sub_ps(_mm256_mul_ps(v1_x, v2_y), _mm256_mul_ps(v1_y, v2_x));
}

void m256_normalize(__m256* x, __m256* y, __m256* z)
{
    __m256 l = _mm256_sqrt_ps(_mm256_add_ps( _mm256_add_ps(
                              _mm256_mul_ps(*x, *x),
                              _mm256_mul_ps(*y, *y)),
                              _mm256_mul_ps(*z, *z)));

    *x = _mm256_div_ps(*x, l);
    *y = _mm256_div_ps(*y, l);
    *z = _mm256_div_ps(*z, l);
}

__m256 m256_dot(__m256 x1, __m256 y1, __m256 z1,
             __m256 x2, __m256 y2, __m256 z2)
{
    __m256 result = _mm256_add_ps(_mm256_add_ps(
                            _mm256_mul_ps(x1, x2),
                            _mm256_mul_ps(y1, y2)),
                            _mm256_mul_ps(z1, z2));

    return result;
}

__m256 m256_sq_len(__m256 x, __m256 y, __m256 z)
{
    return m256_dot(x, y, z, x, y ,z);
}

#endif // !MUH_WIDE_MATH
