#ifndef MUH_SCENES
#define MUH_SCENES

#include <vector>
#include "math.cpp"
#include "render.h"
#include "camera.h"
#include "files.h"

std::vector<MeshID> build_scene(camera* cam, float aspect_ratio)
{
    std::vector<MeshID> list;
#if SCENE == SCENE == SCENE_EMPTY || SCENE == SCENE_MESH_SIMPLE || SCENE == SCENE_MESH_CAMP
    vec3 lookfrom(0,0,2);
    vec3 lookat(0,0,0);
    vec3 vup(0,1,0);
    float dist_to_focus = 1;
    float aperture = 0.01f;
    float vfov = 45;

    MaterialID material_ground = Material_CreateLambertian(color(0.8f, 0.8f, 0.0f));
    MaterialID material_center = Material_CreateLambertian(color(0.1f, 0.2f, 0.5f));
    MaterialID material_left = Material_CreateDielectric(1.5f);
    MaterialID material_right = Material_CreateMetal(color(0.8f, 0.6f, 0.2f), 0.3f);

#if SCENE == SCENE_MESH_CAMP
    vec3 floorVertex1(-50.0f, -0.6f, -50.0f);
    vec3 floorVertex2( 50.0f, -0.65f, -50.0f);
    vec3 floorVertex3( 50.0f, -0.6f,  50.0f);
    vec3 floorVertex4(-50.0f, -0.6f,  50.0f);

    std::vector<triangle> floor_triangles1;
    floor_triangles1.push_back(triangle(floorVertex1, floorVertex4, floorVertex3));
    floor_triangles1.push_back(triangle(floorVertex3, floorVertex2, floorVertex1));
    MeshID floor1 = Mesh_Create(floor_triangles1, material_ground);
    list.push_back(floor1);

    std::vector<triangle> tree_mesh_tris = get_obj_file_tris("res/Rock.obj");
#if 1
    MaterialID material_tree = material_center;
#else
    MaterialID material_tree = Material_CreateDielectric(0.9f, color(1.0f, 0.1f, 0.1f));
#endif
    MeshID tree_mesh_id = Mesh_Create(tree_mesh_tris, material_tree);
    list.push_back(tree_mesh_id);

    color rock_color(0.8f, 0.6f, 0.2f);

    std::vector<triangle> rock_mesh_tris = get_obj_file_tris("res/Rock.obj");
    for (triangle& t : rock_mesh_tris)
    {
        t.a.x -= 2.0;
        t.b.x -= 2.0;
        t.c.x -= 2.0;
    }
    MaterialID material_rock1 = Material_CreateMetal(rock_color, 1.0f);
    MeshID rock_mesh_id = Mesh_Create(rock_mesh_tris, material_rock1);
    list.push_back(rock_mesh_id);

    for (triangle& t : rock_mesh_tris)
    {
        t.a.z -= 2;
        t.b.z -= 2;
        t.c.z -= 2;
    }
    MaterialID material_rock2 = Material_CreateLambertian(color(0.1f, 0.2f, 0.0f));
    MeshID rock_mesh_id2 = Mesh_Create(rock_mesh_tris, material_right);
    list.push_back(rock_mesh_id2);

     /*
    std::vector<triangle> log_mesh_tris = get_obj_file_tris("res/Log.obj");
    for (triangle& t : log_mesh_tris)
    {
        t.a.x -= 2;
        t.b.x -= 2;
        t.c.x -= 2;
    }
    MeshID log_mesh_id = Mesh_Create(log_mesh_tris, material_center);
    list.push_back(log_mesh_id);
    */

     /*
    std::vector<triangle> cube_mes_tris = get_obj_file_tris("res/cube.obj");
    MeshID cube_mes_id = Mesh_Create(cube_mes_tris, material_center);
    list.push_back(cube_mes_id);
    */


#elif SCENE == SCENE_MESH_SIMPLE
    vec3 floorVertex1(-50.0f, -0.6f, -50.0f);
    vec3 floorVertex2( 50.0f, -0.6f, -50.0f);
    vec3 floorVertex3( 50.0f, -0.6f,  50.0f);
    vec3 floorVertex4(-50.0f, -0.6f,  50.0f);

    std::vector<triangle> floor_triangles1;
    floor_triangles1.push_back(triangle(floorVertex1, floorVertex4, floorVertex3));
    floor_triangles1.push_back(triangle(floorVertex3, floorVertex2, floorVertex1));
    MeshID floor1 = Mesh_Create(floor_triangles1, material_ground);
    list.push_back(floor1);

    vec3 wallVertex1(-3.0f, 1.5f,  -5.0f);
    vec3 wallVertex2( 3.0f, 1.5f,  -5.0f);
    vec3 wallVertex3( 3.0f, 0.3f,  -5.0f);
    vec3 wallVertex4(-3.0f, 0.3f,  -5.0f);

    std::vector<triangle> wall1_triangles;
    wall1_triangles.push_back(triangle(wallVertex3, wallVertex2, wallVertex1));
    MeshID wall1 = Mesh_Create(wall1_triangles, material_center);
    list.push_back(wall1);

    std::vector<triangle> wall2_triangles;
    wall2_triangles.push_back(triangle(wallVertex4, wallVertex3, wallVertex1));
    MeshID wall2 = Mesh_Create(wall2_triangles, material_right);
    list.push_back(wall2);

    std::vector<MaterialID> random_materials =
    {
        Material_CreateLambertian(color(0.9f, 0.2f, 0.2f)),
        Material_CreateLambertian(color(0.2f, 0.9f, 0.2f)),
        Material_CreateLambertian(color(0.2f, 0.2f, 0.9f)),
        Material_CreateLambertian(color(0.2f, 0.2f, 0.2f)),
#if 0
        Material_CreateDielectric(random_float() * 2.5f),
        Material_CreateDielectric(random_float() * 2.5f),
        Material_CreateDielectric(random_float() * 2.5f),
        Material_CreateDielectric(random_float() * 2.5f),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateLambertian(color(random_float(), random_float(), random_float())),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
        Material_CreateMetal(color(random_float(), random_float(), random_float()), random_float() * 1.5f),
#endif
    };

    vec3 vertexA(-0.3f, 0.0f, -0.3);
    vec3 vertexB( 0.3f, 0.0f, -0.3);
    vec3 vertexC( 0.3f, 0.0f,  0.3);
    vec3 vertexD(-0.3f, 0.0f,  0.3);
    vec3 vertexE( 0.0f, 0.5f,  0.0);

    std::vector<vec3> offsets =
    {
#if 1
        vec3(-0.5f,   0.3f, -0.3f),
        vec3( 0.5f,   0.3f, -0.3f),
        vec3(-0.5f,  -0.3f, -0.3f),
        vec3( 0.5f,  -0.3f, -0.3f),
#elif 0
        vec3(0.5f,  1.0f, -0.5f),
        vec3(0.5f,  0.5f, -0.5f),
        vec3(0.5f,  0.0f, -0.5f),
        vec3(0.5f, -0.5f, -0.5f),

        vec3(0.0f,  1.0f, -0.5f),
        vec3(0.0f,  0.5f, -0.5f),
        vec3(0.0f,  0.0f, -0.5f),
        vec3(0.0f, -0.5f, -0.5f),

        vec3(-0.5f,  1.0f, -0.5f),
        vec3(-0.5f,  0.5f, -0.5f),
        vec3(-0.5f,  0.0f, -0.5f),
        vec3(-0.5f, -0.5f, -0.5f),

        vec3(-1.05f,  1.0f, -0.25f),
        vec3(-1.05f,  0.5f, -0.25f),
        vec3(-1.05f,  0.0f, -0.25f),
        vec3(-1.05f, -0.5f, -0.25f),
#elif 0
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
        vec3(random_float(-5, 5), random_float(-0.5f, 0.5f), random_float(-5, 0.5f)),
#endif
    };

    for (int i = 0; i < offsets.size(); ++i)
    {
        // Rotate the vertices around the y-axis by 45 degrees
        float angle = (1 * 45.0f) * pi / 180.0f; // convert to radians
        mat3 rotY = mat3(cos(angle), 0, sin(angle), 0, 1, 0, -sin(angle), 0, cos(angle));
        vec3 currentVertexA = rotY * vertexA;
        vec3 currentVertexB = rotY * vertexB;
        vec3 currentVertexC = rotY * vertexC;
        vec3 currentVertexD = rotY * vertexD;
        vec3 currentVertexE = rotY * vertexE;

        vec3 const& pos = offsets[i];

        currentVertexA += pos;
        currentVertexB += pos;
        currentVertexC += pos;
        currentVertexD += pos;
        currentVertexE += pos;

        std::vector<triangle> triangles;
        triangles.push_back(triangle(currentVertexB, currentVertexA, currentVertexE));
        triangles.push_back(triangle(currentVertexC, currentVertexB, currentVertexE));
        triangles.push_back(triangle(currentVertexD, currentVertexC, currentVertexE));
        triangles.push_back(triangle(currentVertexA, currentVertexD, currentVertexE));
        triangles.push_back(triangle(currentVertexA, currentVertexC, currentVertexD));
        triangles.push_back(triangle(currentVertexB, currentVertexC, currentVertexA));

        MeshID new_object = Mesh_Create(triangles, random_materials[ i % random_materials.size()]);
        list.push_back(new_object);
    }
#endif
#elif SCENE == SCENE_MESH_SCENE
    vec3 lookfrom(-25,5,25);
    vec3 lookat(0,5,0);
    vec3 vup(0,1,0);
    float dist_to_focus = 1;
    float aperture = 0.01f;
    float vfov = 45;

    //list = fully_load_obj_file_things("res/scene.obj");
    list = fully_load_obj_file_things("res/scene2.obj");
    //list = fully_load_obj_file_things("res/cottage.obj");
    //list = fully_load_obj_file_things("res/crate.obj");

#elif SCENE == SCENE_MESH_TREX
    vec3 lookfrom(-500,300, 750);
    vec3 lookat(0,100,0);
    vec3 vup(0,1,0);
    float dist_to_focus = 1;
    float aperture = 0.01f;
    float vfov = 45;

    //std::vector<triangle> test_file = get_obj_file_tris("res/t-rex.obj");
    std::vector<triangle> t_rex_mesh_tris = get_obj_file_tris("res/t-rex-simple.obj");

    MaterialID t_rex_material = Material_CreateLambertian(color(0.425f, 0.15f, 0.02f));
    MeshID t_rex_mesh_id = Mesh_Create(t_rex_mesh_tris, t_rex_material);
    list.push_back(t_rex_mesh_id);

    MaterialID ground_material = Material_CreateLambertian(color(0.8f, 0.8f, 0.0f));
    float tile_size = 100;
    vec3 base_ground_vertex1(-tile_size, -0.6f, -tile_size);
    vec3 base_ground_vertex2( tile_size, -0.6f, -tile_size);
    vec3 base_ground_vertex3( tile_size, -0.6f,  tile_size);
    vec3 base_ground_vertex4(-tile_size, -0.6f,  tile_size);

    int tiles_per_side = 3; // i know, it's just half of the total tiles in a side
    for (int i = -tiles_per_side; i < tiles_per_side; ++i)
    {
        for (int j = -tiles_per_side; j < tiles_per_side; ++j)
        {
            vec3 tile_offset = vec3(tile_size * i, 0, tile_size * j);
            vec3 ground_vertex1 = base_ground_vertex1 + tile_offset;
            vec3 ground_vertex2 = base_ground_vertex2 + tile_offset;
            vec3 ground_vertex3 = base_ground_vertex3 + tile_offset;
            vec3 ground_vertex4 = base_ground_vertex4 + tile_offset;

            std::vector<triangle> floor_triangles1;
            floor_triangles1.push_back(triangle(ground_vertex1, ground_vertex4, ground_vertex3));
            floor_triangles1.push_back(triangle(ground_vertex3, ground_vertex2, ground_vertex1));
            MeshID floor1 = Mesh_Create(floor_triangles1, ground_material);
            list.push_back(floor1);
        }
    }
#else
    static_assert(false, "bad scene config");
#endif

    *cam = camera(lookfrom, lookat, vup, vfov, aspect_ratio, aperture, dist_to_focus);

    printf("done creating scene with %zu elements\n", list.size());
    return list;
}

#endif // !MUH_SCENES
