#ifndef MUH_CAMERA_H

#include "math.cpp"

struct camera
{
    vec3 origin, forward, up;
    vec3 horizontal;
    vec3 vertical;
    vec3 lower_left_corner;
    vec3 w, u, v;
    float lens_radius;
    float focus_dist;
    float aspect_ratio;
    float vfov;

    camera() {}

    camera(vec3 origin, vec3 look_at, vec3 up, float vfov, float aspect_ratio, float aperture, float focus_dist)
        : origin(origin), forward((look_at - origin).normalize()), up(up), aspect_ratio(aspect_ratio), vfov(vfov)
    {
        lens_radius = aperture / 2.0f;
        this->focus_dist = focus_dist;
        update_viewport();
    }

    void move(vec3 const& delta)
    {
        origin += delta.x * u + delta.y * v + delta.z * w;
        update_viewport();
    }

    void rotate(vec3 const& delta)
    {
        vec3 angle = (delta.x * u + delta.y * v + delta.z * w) * (pi / 180.0f);

        mat3 rot;

        // x
        rot = mat3(1, 0           ,  0,
                   0, cos(angle.x), -sin(angle.x),
                   0, sin(angle.x),  cos(angle.x));
        forward = rot * forward;
        up = rot * up;

        // y 
        rot = mat3( cos(angle.y), 0, sin(angle.y),
                    0           , 1, 0           ,
                   -sin(angle.y), 0, cos(angle.y));
        forward = rot * forward;
        up = rot * up;

        // z
        rot = mat3(cos(angle.z),-sin(angle.z), 0,
                   sin(angle.z), cos(angle.z), 0,
                   0           , 0           , 1);
        forward = rot * forward;
        up = rot * up;

#if DEBUG_CAMERA || 0
        printf("cam fwd %s\n", forward.c_str());
#endif
    }

    void update_viewport()
    {
        float theta = degrees_to_radians(vfov);
        float h = tan(theta/2.0f);

        float viewport_height = 2.0f * h;
        float viewport_width = aspect_ratio * viewport_height;

        w = unit_vector(-forward);
        u = unit_vector(cross(up, w));
        v = unit_vector(cross(w, u));

        horizontal = focus_dist * viewport_width * u;
        vertical = focus_dist * viewport_height * v;
        lower_left_corner = origin - horizontal/2.0f - vertical/2.0f - focus_dist*w;
    }

    ray get_ray(float s, float t)
    {
        vec3 rd = lens_radius * random_in_unit_disk();
        vec3 offset = u * rd.x + v * rd.y;

        return ray(origin + offset, lower_left_corner + horizontal*s + vertical*t - origin - offset);
    }
};

#endif // !MUH_CAMERA_H
