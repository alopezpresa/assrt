#ifndef MUH_FILES_CPP
#define MUH_FILES_CPP

#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include "performance.cpp"
#include <stb/stb_image.h>
#include <stb/stb_image_write.h>
#include <immintrin.h>
#include <vector>
#include "files.h"
#include "memory.cpp"
#include "render.cpp"
#include "render.h"

void write_image(char const* path, image_buffer const& buffer, image_format format)
{
    SCOPED_TIMER(write_image);

    char* image_name = new char[strlen(path) + 5];

    if (format == image_format_PPM)
    {
        std::ofstream outdata;

        sprintf(image_name, "%s%s", path, ".ppm");
        outdata.open(image_name);

        if (!outdata)
        {
            std::cerr << "Error: file could not be opened" << std::endl;
            exit(1);
        }

        outdata << "P3" << std::endl;
        outdata << buffer.width << " " << buffer.height << std::endl;
        outdata << "255" << std::endl;

        for (int j = buffer.height - 1; j >= 0; --j)
        {
            for (int i = 0; i < buffer.width; ++i)
            {
                bytes_color pixel(buffer.pixels[j*buffer.width + i], buffer.samples_per_pixel);
                outdata << (int)(pixel.r) << " "
                        << (int)(pixel.g) << " "
                        << (int)(pixel.b) << " ";
            }
            outdata << std::endl;
        }

        outdata.close();
    }
    else
    {
        int channels = 3;
        uint8_t* data = new uint8_t[buffer.height * buffer.width * channels];
        int index = 0;

        for (int j = buffer.height - 1; j >= 0; --j)
        {
            for (int i = 0; i < buffer.width; ++i)
            {
                bytes_color pixel(buffer.pixels[j*buffer.width + i], buffer.samples_per_pixel);

                for (int c = 0; c < channels; ++c)
                {
                    data[index++] = pixel.e[c];
                }
            }
        }

        if (format == image_format_PNG)
        {
            sprintf(image_name, "%s%s", path, ".png");
            stbi_write_png(image_name, buffer.width, buffer.height, channels, data, buffer.width * channels);
        }
        else if (format == image_format_JPG)
        {
            sprintf(image_name, "%s%s", path, ".jpg");
            stbi_write_jpg(image_name, buffer.width, buffer.height, channels, data, 100);
        }
        else
        {
            sprintf(image_name, "%s%s", path, ".bmp");
            stbi_write_bmp(image_name, buffer.width, buffer.height, channels, data);
        }

        delete [] data;
    }

    delete [] image_name;
}

void obj_file_count_vert_and_faces(const char* fileData, int fileSize, int* vertexCount, int* faceCount)
{
    __m256i vV = _mm256_set1_epi8('v');
    __m256i vSpace = _mm256_set1_epi8(' ');
    __m256i vF = _mm256_set1_epi8('f');

    *vertexCount = 0;
    *faceCount = 0;

    char buffer[32];
    int bytes_read;

    for (int offset = 0; offset < fileSize - 32; offset += 32)
    {
        __m256i vData = _mm256_loadu_si256((__m256i*)(fileData + offset));
        __m256i vData2 = _mm256_loadu_si256((__m256i*)(fileData + offset + 1));
        __m256i vFollowedBySpace = _mm256_cmpeq_epi8(vData2, vSpace);

        // Compare characters with 'v'
        __m256i vCompareV = _mm256_and_si256(_mm256_cmpeq_epi8(vData, vV), vFollowedBySpace);

        // Check if any 'v' characters are found
        int maskV = _mm256_movemask_epi8(vCompareV);
        int countV = _mm_popcnt_u32(maskV);
        *vertexCount += countV;
        //
        // Compare characters with 'f'
        __m256i vCompareF = _mm256_and_si256(_mm256_cmpeq_epi8(vData, vF), vFollowedBySpace);

        // Check if any 'f' characters are found
        int maskF = _mm256_movemask_epi8(vCompareF);
        int countF = _mm_popcnt_u32(maskF);
        *faceCount += countF;
    }
}

std::vector<triangle> get_obj_file_tris(const char* filepath)
{
    printf("preparing to open f: %s\n", filepath);

    std::vector<vec3> vertices;
    std::vector<vec3> texture_coords;
    std::vector<vec3> normals;
    std::vector<triangle> triangles;

    FILE* f;
    int maxLineLenght = 255;
    char line[maxLineLenght];

    f = fopen(filepath, "r");

    if (f)
    {
        // Simple parsing, no normals or texture coordinate support
        // It expects a single object per file with no materials
        enum ParsingStep
        {
            ParsingStep_None,
            ParsingStep_ReadingVertex,
            ParsingStep_ReadingVertexTexture,
            ParsingStep_ReadingNormals,
            ParsingStep_ReadingFace,
        };

        int parsingStep = ParsingStep_None;
        int lineNo = 0;

        while (fgets(line, maxLineLenght, f))
        {
            ++lineNo;
            int valuePos = 0;

            switch (line[0])
            {
                case 'v':
                    {
                        switch (line[1])
                        {
                            case ' ':
                                parsingStep = ParsingStep_ReadingVertex;
                                break;
                            case 't':
                                parsingStep = ParsingStep_ReadingVertexTexture;
                                ++valuePos;
                                break;
                            case 'n':
                                parsingStep = ParsingStep_ReadingNormals;
                                ++valuePos;
                                break;
                        }

                        while (line[++valuePos] == ' ');
                    }
                    break;

                case 'f':
                    {
                        parsingStep = ParsingStep_ReadingFace;

                        while (line[++valuePos] == ' ');
                    }
                    break;

                case '#':
                case 'o':
                case 's':
                case '\n':
                case '\r':
                case 'm': // TODO: something related to materials, will need to add support for this later
                case 'g': // TODO: what's this? i don't know
                case 'u': // TODO: what's this? i don't know
                    continue;

                default:
                    {
                        printf("bad obj read: %s, unexpected char '%c' (%d) at line %d: %s\n", filepath, line[0], line[0], lineNo, line);
                        assert(false);
                    }
            }

            switch (parsingStep)
            {
                case ParsingStep_ReadingVertex:
                case ParsingStep_ReadingVertexTexture:
                case ParsingStep_ReadingNormals:
                    {
                        vec3 vertex;
                        int read = sscanf(line+valuePos, "%f %f %f", &vertex.x, &vertex.y, &vertex.z);
                        switch (parsingStep)
                        {
                            case ParsingStep_ReadingVertex:
                                printf("vertex parsing: read %d float values from at line %d: %s\n", read, lineNo, line);
                                assert(read == 3);
                                vertices.push_back(vertex);
                                break;
                            case ParsingStep_ReadingVertexTexture:
                                printf("texture coord parsing: read %d float values from at line %d: %s\n", read, lineNo, line);
                                assert(1 <= read && read <= 3);
                                texture_coords.push_back(vertex);
                                break;
                            case ParsingStep_ReadingNormals:
                                printf("%s\n", line+valuePos);
                                printf("normal parsing: read %d float values from at line %d: %s\n", read, lineNo, line);
                                assert(read == 3);
                                normals.push_back(vertex);
                                break;
                        }
                    }
                    break;

                case ParsingStep_ReadingFace:
                    {
                        // face format:
                        // f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3
#define VERTEX_DATA_CHUNK 64
                        char vertexDatas[VERTEX_DATA_CHUNK*3];
                        int readData = sscanf(line+valuePos, "%s %s %s", &vertexDatas[VERTEX_DATA_CHUNK*0],
                                                                         &vertexDatas[VERTEX_DATA_CHUNK*1],
                                                                         &vertexDatas[VERTEX_DATA_CHUNK*2]);

                        printf("face parsing: read %d data values from at line %d: %s\n", readData, lineNo, line);

                        int v[3], vt[3], n[3];

                        for (int i = 0; i < 3; ++i)
                        {
                            v[i] = vt[i] = n[i] = -666;
                            int read = sscanf(&vertexDatas[VERTEX_DATA_CHUNK*i], "%d/%d/%d", &v[i], &vt[i], &n[i]);
                            printf("face parsing: read %d int values for vertex %d:\n", read, i);
                            printf("v: %d, vt: %d, n: %d\n", v[i], vt[i], n[i]);
                        }

                        // NOTE: we don't use normals, it seems it's faster to calculate them than to store and load
                        triangle t(vertices[v[0]-1], vertices[v[1]-1], vertices[v[2]-1], vec3::zero(), vec3::zero(), vec3::zero());
                        triangles.push_back(t);
                    }
                    break;
            }
        }
    }
    else
    {
        printf("Failed to open f: %s\n", filepath);
    }

    fclose(f);

    return triangles;
}

std::string folder_path_form_full_path(std::string reference_file_path, std::string target_file)
{
#if windows
    auto final_slash = reference_file_path.find_last_of('\\');
#else
    auto final_slash = reference_file_path.find_last_of('/');
#endif
    std::string result = reference_file_path.substr(0, final_slash+1) + target_file;
    return result;
}

std::unordered_map<std::string, MaterialID> load_obj_materials_library(const char* filepath)
{
    printf("material lib path: %s\n", filepath);
    std::unordered_map<std::string, MaterialID> results;

    FILE* f;
    int maxLineLenght = 255;
    char line[maxLineLenght];

    f = fopen(filepath, "r");

    if (f)
    {
        int lineNo = 0;
        MaterialID material_id;

        while (fgets(line, maxLineLenght, f))
        {
            ++lineNo;
            switch (line[0])
            {
                case '\n':
                case '\r':
                    material_id = -1;
                case '#':
                    break;

                case 'n': // it seems safe to assume it's "newmtl"
                    {
                        char material_name[256];
                        int read = sscanf(line, "newmtl %s", material_name);
                        material_id = results[material_name] = Material_CreateLambertian(color(1,0,1));
                    }

                    break;
                default:

                    if (strncmp(line, "Ka", 2) == 0) // ambient color
                    {
                    }
                    else if (strncmp(line, "Kd", 2) == 0) // diffuse color
                    {
                        Material* material = Material_Get(material_id);
                        int read = sscanf(line, "Kd %f %f %f", &(material->albedo.x), &(material->albedo.y), &(material->albedo.z));
                        printf("updated material diffuse color: %f, %f, %f (read %d values from '%s').\n", material->albedo.x, material->albedo.y, material->albedo.z, read, line);
                    }
                    else if (strncmp(line, "Ke", 2) == 0) // emissive color
                    {
                    }
                    else if (strncmp(line, "Ks", 2) == 0) // specular color
                    {
                    }
                    else if (strncmp(line, "Ns", 2) == 0) // specular exponent
                    {
                    }
                    else if (strncmp(line, "Ni", 2) == 0) // optical density/index of refraction, 0.001-10
                    {
                    }
                    else if (strncmp(line, "d", 1) == 0 || strncmp(line, "Tr", 2) == 0) // disolve, complex thing, might be a float might be bunch o things
                    {
                    }
                    else if (strncmp(line, "illum", 5) == 0) // illumination mode, 0-10
                    {
                    }
                    else if (strncmp(line, "map_Kd", 6) == 0) // illumination mode, 0-10
                    {
                        char texture_name[255];
                        int read = sscanf(line, "map_Kd %s", texture_name);

                        std::string texture_path = folder_path_form_full_path(filepath, texture_name);

                        Material* material = Material_Get(material_id);
                        material->diffuse_map = Texture_Load(texture_path.c_str());
                        printf("update material diffuse map with texture from %s\n", texture_path.c_str());
                    }
                    else
                    {
                        printf("unexpected value at line %d: %s", lineNo, line);
                        exit(0);
                    }
                    break;
            }
        }
    }
    else
    {
        printf("Failed to open f: %s\n", filepath);
    }

    fclose(f);


    for (auto const& m : results)
    {
        printf("created material '%s' with id %d\n", m.first.c_str(), m.second);
        
    }

    return results;
}

std::vector<MeshID> fully_load_obj_file_things(const char* filepath)
{
    std::vector<MeshID> results;

    printf("preparing to open f: %s\n", filepath);

    std::vector<vec3> vertices;
    std::vector<vec3> texture_coords;
    std::vector<vec3> normals;
    std::unordered_map<std::string, MaterialID> materials_by_name;
    std::unordered_map<MeshID, MaterialID> meshes_materials;

    std::vector<triangle> current_mesh_triangles;
    MaterialID current_mesh_material = -1;

    FILE* f;
    int maxLineLenght = 255;
    char line[maxLineLenght];

    f = fopen(filepath, "r");

    if (f)
    {
        int lineNo = 0;

        while (fgets(line, maxLineLenght, f))
        {
            ++lineNo;

            switch (line[0])
            {
                case 'v':
                    {
                        switch (line[1])
                        {
                            case ' ':
                                {
                                    vec3 vertex; int read = sscanf(line, "v %f %f %f", &vertex.x, &vertex.y, &vertex.z);
                                    printf("vertex parsing: read %d float values from at line %d: %s\n", read, lineNo, line);
                                    assert(read == 3);
                                    vertices.push_back(vertex);
                                }
                                break;
                            case 't':
                                {
                                    vec3 vertex = vec3::zero();
                                    int read = sscanf(line, "vt %f %f %f", &vertex.x, &vertex.y, &vertex.z);
                                    printf("texture coord parsing: read %d float values (%f, %f, %f) at line %d: %s\n", read, vertex.x, vertex.y, vertex.z, lineNo, line);
                                    assert(1 <= read && read <= 3);
                                    texture_coords.push_back(vertex);
                                }
                                break;
                            case 'n':
                                {
                                    vec3 vertex;
                                    int read = sscanf(line, "vn %f %f %f", &vertex.x, &vertex.y, &vertex.z);
                                    printf("normal parsing: read %d float values from at line %d: %s\n", read, lineNo, line);
                                    assert(read == 3);
                                    normals.push_back(vertex);
                                }
                                break;
                        }
                    }
                    break;

                case 'f':
                    {
                        // TODO: support non-triangular faces!
                        // face format:
                        // f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3
#define VERTEX_DATA_CHUNK 64
                        char vertexDatas[VERTEX_DATA_CHUNK*3];
                        int readData = sscanf(line, "f %s %s %s", &vertexDatas[VERTEX_DATA_CHUNK*0],
                                                                  &vertexDatas[VERTEX_DATA_CHUNK*1],
                                                                  &vertexDatas[VERTEX_DATA_CHUNK*2]);

                        printf("face parsing: read %d data values from at line %d: %s\n", readData, lineNo, line);

                        int v[3], vt[3], n[3];

                        for (int i = 0; i < 3; ++i)
                        {
                            v[i] = vt[i] = n[i] = -666;
                            int read = sscanf(&vertexDatas[VERTEX_DATA_CHUNK*i], "%d/%d/%d", &v[i], &vt[i], &n[i]);
                            printf("face parsing: read %d int values for vertex %d:\n", read, i);
                            printf("v: %d, vt: %d, n: %d\n", v[i], vt[i], n[i]);
                        }

                        vec3 a = vertices[v[0]-1];
                        vec3 b = vertices[v[1]-1];
                        vec3 c = vertices[v[2]-1];

                        vec3 ta = vt[0] >= 1 ? texture_coords[vt[0]-1] : vec3::zero();
                        vec3 tb = vt[1] >= 1 ? texture_coords[vt[1]-1] : vec3::zero();
                        vec3 tc = vt[2] >= 1 ? texture_coords[vt[2]-1] : vec3::zero();
                        
                        printf("loaded face data - a: %s\n", a.c_str());
                        printf("loaded face data - b: %s\n", b.c_str());
                        printf("loaded face data - c: %s\n", c.c_str());
                        printf("loaded face data - ta: %f, %f, %f\n", ta.x, ta.y, ta.z);
                        printf("loaded face data - tb: %f, %f, %f\n", tb.x, tb.y, tb.z);
                        printf("loaded face data - tc: %f, %f, %f\n", tc.x, tc.y, tc.z);

                        // NOTE: we don't use normals, it seems it's faster to calculate them than to store and load
                        triangle t(a, b, c, ta, tb, tc);
                        current_mesh_triangles.push_back(t);
                    }
                    break;

                case '#':
                case 'o':
                case 's':
                case '\n':
                case '\r':
                case 'g': // TODO: what's this? i don't know
                    continue;
                case 'm':
                    {
                        // THIS IS *SO* UGLY!
                        // I don't really like mixing things this much, but I just want to 
                        // get things done at the moment and I always knew strings and memory
                        // management are not the focus of this project. The goal is to learn
                        // and do cool stuff around path tracing

                        printf("material data: %s\n", line);
                        char helper_buffer[256];
                        int read = sscanf(line, "mtllib %s", helper_buffer);
                        std::string materials_lib_path = folder_path_form_full_path(filepath, helper_buffer);
                        auto loaded_materials = load_obj_materials_library(materials_lib_path.c_str());
                        materials_by_name.insert(loaded_materials.begin(), loaded_materials.end());
                    }
                    break;
                case 'u':
                    {
                        printf("material line data: %s\n", line);

                        if (current_mesh_triangles.size() > 0)
                        {
                            if (current_mesh_material < 0)
                            {
                                current_mesh_material = Material_CreateLambertian(color(1, 0, 1));
                            }

                            MeshID current_mesh_id = Mesh_Create(current_mesh_triangles, current_mesh_material);
                            results.push_back(current_mesh_id);

                            current_mesh_triangles.clear();
                            current_mesh_material = -1;
                        }

                        assert( current_mesh_triangles.size() == 0 && current_mesh_material < 0);

                        char material_name[256];
                        int read = sscanf(line, "usemtl %s", material_name);
                        assert(read == 1);
                        current_mesh_material = materials_by_name[material_name];
                    }

                    break;

                default:
                    {
                        printf("bad obj read: %s, unexpected char '%c' (%d) at line %d: %s\n", filepath, line[0], line[0], lineNo, line);
                        assert(false);
                    }
            }
        }
    }
    else
    {
        printf("Failed to open f: %s\n", filepath);
    }

    if (current_mesh_triangles.size() > 0)
    {
        if (current_mesh_material < 0)
        {
            current_mesh_material = Material_CreateLambertian(color(1, 0, 1));
        }

        MeshID current_mesh_id = Mesh_Create(current_mesh_triangles, current_mesh_material);
        results.push_back(current_mesh_id);

        current_mesh_triangles.clear();
        current_mesh_material = -1;
    }

    fclose(f);

    return results;
}

#endif // !MUH_FILES_CPP
